
'use strict';

// load modules
var gulp = require('gulp');
var gulp_watch = require('gulp-watch');
var htmlreplace = require('gulp-html-replace');
var sass = require('gulp-sass');
var usemin = require('gulp-usemin');
var uglify = require('gulp-uglify');
var minifyHtml = require('gulp-minify-html');
var minifyCss = require('gulp-minify-css');
var rev = require('gulp-rev');
var rimraf = require('gulp-rimraf');
var connect = require('gulp-connect');
var inlineAngularTemplates = require('gulp-inline-angular-templates');
var runSequence = require('run-sequence');
var src = process.cwd();
console.log('cwd:', src);
// set up connect task for spawning a web server for development
gulp.task('connect-dev', function() { 
	connect.server({
		root: 'app',
		host: '0.0.0.0',
		port: 10000,
		livereload: true,
		middleware: function(){
			return [
				function(req, res, next)
				{
					console.log(req.socket.remoteAddress, req.method, req.originalUrl);
					next();
				}
			];
		}
	});
});

// set up connect task for spawning a web server for testing a distributed version
gulp.task('connect-dist', function() {
	connect.server({
		root: 'dist',
		host: '0.0.0.0',
		port: 10000
	});
});

// minification task based on references of html
gulp.task('usemin', function() {
	return gulp.src('./app/index.html')
	.pipe(htmlreplace({
		'css-development': [], // remove tags for development purposes
		'js-development': [],
		'js-mockup': []
	}))
	.pipe(usemin({
		css: [ minifyCss(), rev() ],
		html: [ function(){ return minifyHtml({ empty: true }) } ],
		jsVendor: [ uglify(), rev() ],
		jsApp: [ uglify(), rev() ],
		jsConfig: [ ]
	}))
	.pipe(gulp.dest('./dist/'));
});

gulp.task('minify:views', function(){
	return gulp.src('./dist/views/**/*.html')
	.pipe(usemin({
		html: [ function(){ return minifyHtml({ empty: true }) } ]
	}))
	.pipe(gulp.dest('./dist/views/'));
});

gulp.task('append:views', function() {
	return gulp.src('./dist/views/**/*.html')
	.pipe(inlineAngularTemplates('./dist/index.html',{
        base: 'dist/views', 
        prefix: '/views', 
        selector: 'body', 
        method: 'append', 
        unescape: { 
            '&lt;': '<',
            '&gt;': '>',
            '&apos;': '\'',
            '&amp;': '&'
        }
    }))
	.pipe(gulp.dest('./dist/'))
});

// minification task based on references of html
gulp.task('usemin:testing', function() {
	return gulp.src('./app/index.html')
	.pipe(htmlreplace({
		'css-development': [], // remove tags for development purposes
		'js-development': []
	}))
	.pipe(usemin({
		css: [ minifyCss(), rev() ],
		html: [ minifyHtml({ empty: true }) ],
		js: [ uglify(), rev() ],
	}))
	.pipe(gulp.dest('./dist/'));
});

// .scss compile task
gulp.task('sass', function () {
	return gulp.src('./app/sass/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./app/css'));
});

// copy files for distributed version
gulp.task('copy:views', function(){
	return gulp.src('./app/views/**/*.html')
		.pipe(gulp.dest('./dist/views/'));
});

gulp.task('copy:fonts', function(){
	return gulp.src('./app/fonts/**/*.*')
		.pipe(gulp.dest('./dist/fonts/'));
});

gulp.task('copy:images', function(){
	return gulp.src('./app/img/**/*.*')
		.pipe(gulp.dest('./dist/img/'));
});

// watch for changes
gulp.task('watch', function () {
	gulp.watch('app/sass/**/*.scss', {cwd: src}, ['sass']);
	gulp.watch(['app/**/*.js','app/**/*.html','app/**/*.css'], {cwd: src}).on('change', function(file){
		gulp.src(file.path).pipe(connect.reload()); // reload the pages in browsers which are connected to this host
	});
});

gulp.task('clean:dist', function(){
	return gulp.src(['./dist/'], { read: false })
	.pipe(rimraf());
});

gulp.task('clean:dist:views', function(){
	return gulp.src(['./dist/views'], { read: false })
	.pipe(rimraf());
});

gulp.task('develop', ['sass', 'connect-dev', 'watch']);

gulp.task('compile-dev', ['sass']);

gulp.task('compile-release', ['clean:dist'], function(cb){
	runSequence('clean:dist','sass','usemin','copy:views','minify:views',['append:views','copy:fonts','copy:images'],'clean:dist:views', cb);
});

gulp.task('deploy', function(cb){
	runSequence('clean:dist','sass','usemin','copy:views','minify:views',['append:views','copy:fonts','copy:images'],'clean:dist:views','connect-dist', cb);
});

gulp.task('default', ['develop']); // run tasks for development by default
