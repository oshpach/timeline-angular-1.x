
angular
.module('history-timeline')
.config(['Config',function(Config) {
    Config.FacebookAppId = document.domain === 'localhost' ? '1771133999839586' : '1771309243155395';
}])
.config(['$stateProvider','Config',function($stateProvider, Config){
    $stateProvider.state('setApiServer', {
        url: '/use-api/:host/:port',
        controller: ['$state', '$stateParams', '$window',function($state, $stateParams, $window){
            $window.localStorage.apiTestServer = '//' + $stateParams.host + ':' + $stateParams.port;
            $window.location.href = $state.href('entrance');
            $window.location.reload();
        }]
    })
    
    $stateProvider.state('resetApiServer', {
        url: '/reset-api',
        controller: ['$state', '$stateParams', '$window',function($state, $stateParams, $window){
            delete $window.localStorage.apiTestServer;
            $window.location.href = $state.href('entrance');
            $window.location.reload();
        }]
    })
}])
.run(['$window','Config',function($window, Config){
    if($window.localStorage.apiTestServer)
        Config.ApiServer = $window.localStorage.apiTestServer;
}]);