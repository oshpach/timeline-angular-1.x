(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .service('TimelineService', TimelineService);
    
    TimelineService.$inject = ['$q','$http', '$window', 'ApiUrl'];
    function TimelineService($q, $http, $window, ApiUrl){
        var self = this;
        
        self.getTimeline = getTimeline;
        self.getTimelineBackground = getTimelineBackground;
        self.getEvents = getEvents;
        self.getEventById = getEventById;
        self.getContents = getContents;
        self.getContentById = getContentById;
        self.getComments = getComments;
        self.toggleLikeContent = toggleLikeContent;
        self.addEvent = addEvent;
        self.addContent = addContent;
        self.addComment = addComment;
        self.shareContentViaMail = shareContentViaMail;
        self.reportToAdmin = reportToAdmin;
        self.searchEvents = searchEvents;
        self.searchEventsEx = searchEventsEx;
        
        function getTimeline(cursor, requireYear)
        {
            return $http
            .get(ApiUrl.GetTimeline, {
                params: {
                    cursor: cursor,
                    //year: requireYear
                }
            })
            .then(function success(response){
                return response.data;
            }, function fail(response){
                
            });
        }
        
        function getTimelineBackground()
        {
            return $http
            .get(ApiUrl.GetBackground)
            .then(function success(response){
                return response.data;
            }, function fail(response){
                
            });
        }
        
        function getEvents(year, cursor)
        {
            return $http
            .get(ApiUrl.GetEvents, {
                params: {
                    year: year,
                    cursor: cursor
                }
            })
            .then(function success(response){
                return response.data;
            }, function fail(response){
                
            });
        }
        
        function searchEvents(contains)
        {
            return $http
            .get(ApiUrl.Search, {
                params: {
                    query: contains
                }
            })
            .then(function success(response){
                return response.data;
            }, function fail(response){
                
            });
        }
        
        function searchEventsEx(query, cursor)
        {
            return $http
            .get(ApiUrl.SearchEvents, {
                params: {
                    query: query,
                    cursor: cursor
                }
            })
            .then(function success(response){
                return response.data;
            }, function fail(response){
                
            });
        }
        
        function getEventById(id)
        {
            return $http
            .get(ApiUrl.GetEventById, {
                params: {
                    id: id
                }
            })
            .then(function success(response){
                return response.data;
            }, function fail(response){
                
            });
        }
        
        function getContents(event, cursor)
        {
            return $http
            .get(ApiUrl.GetContents, {
                params: {
                    event: event,
                    cursor: cursor
                }
            })
            .then(function success(response){
                return response.data;
            }, function fail(response){
                
            });
        }
        
        function getContentById(id, type)
        {
            return $http
            .get(ApiUrl.GetContentById, {
                params: {
                    content: id,
                    type: type
                }
            })
            .then(function success(response){
                return response.data;
            }, function fail(response){
                
            });
        }
        
        function shareContentViaMail(eventId, contentId, type, recipientName, recipientEmail, message)
        {
            return $http
            .post(ApiUrl.ShareViaMail, {
                event: eventId,
                content: contentId,
                type: type,
                recipientName: recipientName,
                recipientEmail: recipientEmail,
                message: message
            })
            .then(function success(response){
                return response.status;
            }, function fail(response){
                return response.status;
            });
        }
        
        function reportToAdmin(eventId, contentId, type, topic, message)
        {
            return $http
            .post(ApiUrl.ReportToAdmin, {
                event: eventId,
                content: contentId,
                type: type,
                topic: topic,
                message: message
            })
            .then(function success(response){
                return response.status;
            }, function fail(response){
                return response.status;
            });
        }
        
        
        
        function getComments(contentId, type, cursor)
        {
            return $http
            .get(ApiUrl.GetComments, {
                params: {
                    content: contentId,
                    type: type,
                    cursor: cursor
                }
            })
            .then(function success(response){
                return response.data;
            }, function fail(response){
                
            });
        }
        
        function toggleLikeContent(eventId, contentId, type)
        {
            return $http
            .post(ApiUrl.ToggleLikeContent, {
            }, {
                params: {
                    event: eventId,
                    content: contentId,
                    type: type
                }
            })
            .then(function success(response){
                return response.status;
            }, function fail(response){
                return response.status;
            });
        }
        
        function addEvent(name, year, month, day)
        {
            throw new Error('Disabled! Use "event" as json in addContent()');
            return $http
            .post({
                name: name,
                year: year,
                month: month,
                day: day
            })
            .then(function success(response){
                return response.data;
            }, function fail(response){
                return {
                    errorCode: response.status
                };
            });
        }
        
        function addContent(event, headline, details, credits, media)
        {
            var formData = new $window.FormData();
            formData.append("event", angular.toJson(event));
            formData.append("details", details);
            formData.append("credits", credits);
            formData.append("media", media);
            return $http({
                url: ApiUrl.AddContent,
                method: "POST",
                data: formData,
                transformRequest: angular.identity,
                headers: {"Content-Type": undefined}
            })
            .then(function success(response){
                return response.status;
            }, function fail(response){
                return response.status;
            });
        }
        
        function addComment(eventId, contentId, type, comment)
        {
            return $http
            .post(ApiUrl.AddComment, {
                event: eventId,
                content: contentId,
                type: type,
                comment: comment
            })
            .then(function success(response){
                return response.status;
            }, function fail(response){
                return response.status;
            });
        }
        
    }
})();