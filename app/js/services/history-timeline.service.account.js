(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .service('AccountService', AccountService);
    
    AccountService.$inject = ['$q', '$http', '$window', 'Facebook', 'Config', 'ApiUrl'];
    function AccountService($q, $http, $window, Facebook, Config, ApiUrl){
        var self = this;
        
        self.login = login;
        self.logout = logout;
        self.signup = signup;
        self.signupFb = signupFb;
        self.loginFb = loginFb;
        self.restorePassword = restorePassword;
        self.hasAuthorisationCookie = hasAuthorisationCookie;
        
        function login(email, password, rememberMe)
        {
            return $http
            .post(ApiUrl.LogIn, {
                email: email,
                password: password,
                rememberMe: !!rememberMe
            })
            .then(function success(response){
                $window.localStorage[Config.AppStorageConfigPrefix+'AUTHORISED'] = '1';
                return response.status;
            }, function fail(response){
                return response.status;
            })
        }
        
        function signup(name, email, password)
        {
            return $http
            .post(ApiUrl.SignUp, {
                name: name,
                email: email,
                password: password
            })
            .then(function success(response){
                return response.status;
            }, function fail(response){
                return response.status;
            })
        }
        
        function loginFb()
        {
            return $q(function(resolve){
                Facebook.login(function(response) {
                    resolve(response);
                }, {
                    scope: "email public_profile"
                });
            })
            .then(function(response){
                if(response.status === 'connected')
                    return $http
                    .post(ApiUrl.SignUpFacebook, response.authResponse)
                    .then(function success(apiResponse){
                        $window.localStorage[Config.AppStorageConfigPrefix+'AUTHORISED'] = '1';
                        return apiResponse.status;
                    }, function fail(apiResponse){
                        return apiResponse.status;
                    });
                console.log(response);
            });
        }
        
        function signupFb()
        {
            return loginFb();
        }
        
        function logout()
        {
            return $http
            .post(ApiUrl.LogOut, {})
            .then(function success(response){
                delete $window.localStorage[Config.AppStorageConfigPrefix+'AUTHORISED'];
                return response.status;
            }, function fail(response){
                return response.status;
            });
        }
        
        function restorePassword(email)
        {
            return $http
            .post(ApiUrl.RestorePassword, {
                email: email
            })
            .then(function success(response){
                return response.status;
            }, function fail(response){
                return response.status;
            });
        }
        
        function hasAuthorisationCookie()
        {
            // api uses HTTP-only cookies, can't read them in javascript  
            return $q.resolve($window.localStorage.hasOwnProperty(Config.AppStorageConfigPrefix+'AUTHORISED'));
        }
    }
})();