(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .service('AppService', AppService);
    
    AppService.$inject = ['$q', '$http', '$window', '$state', 'Config', 'ApiUrl', 'AccountService'];
    function AppService($q, $http, $window, $state, Config, ApiUrl, AccountService){
        var self = this;
        self.getLanguages = getLanguages;
        self.getLanguageData = getLanguageData;
        self.getDomains = getDomains;
        self.getDomainById = getDomainById;
        self.selectDomain = selectDomain;
        self.reset = reset;
        self.loadSavedDomain = loadSavedDomain;
        
        function getLanguages()
        {
            return $http
            .get(ApiUrl.GetLanguages)
            .then(function success(response){
                return response.data;
            }, function fail(response){
                return response.data;
            })
        }
        
        function getLanguageData(langId)
        {
            return $http
            .get(ApiUrl.GetLanguageData, {
                params: {
                    language: langId
                }
            })
            .then(function success(response){
                return response.data;
            }, function fail(response){
                return response.data;
            });
        }
        
        function getDomains(langId)
        {
            return $http
            .get(ApiUrl.GetDomains, {
                params: {
                    language: langId
                }
            })
            .then(function success(response){
                return response.data;
            }, function fail(response){
                return response.data;
            });
        }
        
        function getDomainById(langId, domainId)
        {
            return $http
            .get(ApiUrl.GetDomainById, {
                params: {
                    language: langId,
                    domain: domainId
                }
            })
            .then(function success(response){
                return response.data;
            }, function fail(response){
                return response.data;
            });
        }
        
        function loadSavedDomain()
        {
            var langId = $window.localStorage[Config.AppStorageConfigPrefix + 'LANG_ID'];
            var domainId = $window.localStorage[Config.AppStorageConfigPrefix + 'DOMAIN_ID'];
            return AccountService.hasAuthorisationCookie()
            .then(function(authorised){
                if(langId != null && domainId != null && ($window.localStorage[Config.AppStorageConfigPrefix + 'PERMANENT_DOMAIN'] === '1' || authorised))
                    self.currentDomain = {
                        langId: langId,
                        domainId: domainId
                    };
            });
        }
        
        function selectDomain(langId, domainId, rememberChoice)
        {
            $window.localStorage[Config.AppStorageConfigPrefix + 'LANG_ID'] = langId;
            $window.localStorage[Config.AppStorageConfigPrefix + 'DOMAIN_ID'] = domainId;
            rememberChoice && ($window.localStorage[Config.AppStorageConfigPrefix + 'PERMANENT_DOMAIN'] = '1');
            self.currentDomain = {
                langId: langId,
                domainId: domainId
            };
            return $q.resolve();
        }
        
        function reset(force)
        {
            if(!force && $window.localStorage[Config.AppStorageConfigPrefix + 'PERMANENT_DOMAIN']) return $q.resolve();
            self.currentDomain = null;
            $state.appData = {};
            delete $window.localStorage[Config.AppStorageConfigPrefix + 'LANG_ID'];
            delete $window.localStorage[Config.AppStorageConfigPrefix + 'DOMAIN_ID'];
            delete $window.localStorage[Config.AppStorageConfigPrefix + 'PERMANENT_DOMAIN'];
            return $q.resolve();
        }
    }
})();