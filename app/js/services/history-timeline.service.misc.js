(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .service('MiscService', MiscService);
    
    MiscService.$inject = ['$q', '$http', 'ApiUrl'];
    function MiscService($q, $http, ApiUrl){
        var self = this;
        self.contactToAdmin = contactToAdmin;
        self.getInfo = getInfo;
        
        function contactToAdmin(fullName, phoneNumber, email, topic, additionalDetails)
        {
            return $http
            .post(ApiUrl.ContactToAdministration, {
                fullName: fullName,
                phoneNumber: phoneNumber,
                email: email,
                topic: topic,
                additionalDetails: additionalDetails
            })
            .then(function success(response){
                return response.status;
            }, function failure(response){
                return response.status;
            });
        }
        
        function getInfo(page)
        {
            return $http
            .get(ApiUrl.GetStaticPage, {
                params: {
                    name: page
                }
            })
            .then(function success(response){
                return response.data;
            }, function failure(response){
            });
        }
    }
})();