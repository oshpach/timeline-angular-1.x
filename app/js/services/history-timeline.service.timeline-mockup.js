(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .run(MockupTimelineService);
    
    MockupTimelineService.$inject = ['$q','$window','$state','$timeout','TimelineService'];
    function MockupTimelineService($q, $window, $state, $timeout, TimelineService){
        
        var self = TimelineService;
        if($window.localStorage.apiTestServer) return;
        self.getTimeline = getTimelineSlow;
        self.getTimelineBackground = getTimelineBackground;
        self.getEvents = getEventsSlow;
        self.getEventById = getEventById;
        self.searchEvents = searchEvents;
        self.searchEventsEx = searchEventsEx;
        self.getContents = getContents;
        self.getContentById = getContentById;
        self.toggleLikeContent = toggleLikeContent;
        self.addEvent = addEvent;
        self.addContent = submitContent;
        self.addComment = postComment;
        self.getComments = getComments;
        self.$state = $state;
        self.shareContentViaMail = shareContentViaMail;
        self.reportToAdmin = reportToAdmin;
        
        self.sampleContents = [];
        self.sampleEvents = [];
        self.sampleComments = [];
        
        $window.timelineService = self;
        
        function getTimelineSlow(cursor, requireYear)
        {
            return $q(function(resolve){
                $timeout(function(){
                    getTimeline(cursor, requireYear)
                    .then(function(result){
                        resolve(result);
                    });
                }, 1000);
            })
        }
        
        function getTimeline(cursor, requireYear)
        {
            var maxItems = undefined;
            return $q(function(resolve){
                resolve(self.sampleEvents.map(function(x){
                    return x.year;
                })
                .reduce(function(arr, i){
                    arr.indexOf(i) == -1 && arr.push(i);
                    return arr;
                }, [])
                /*.filter(function(year){
                    if(requireYear && !cursor)
                    {
                        return (year|0) >= (requireYear|0);
                    }
                    else
                    if(cursor)
                    {
                        return (year|0) < (cursor|0);
                    }
                    return true;
                })*/
                .sort(function(a, b){
                    return (b|0) - (a|0);
                })
                .slice(0, (!requireYear || cursor) ? maxItems : undefined))
            })
            .then(function(arr){
                arr = arr.map(function(i){
                    return {
                        year: i,
                        thumbnails: [],
                        events: [],
                        hasMoreEvents: false
                    };
                });
                
                var promise = $q.resolve();
                arr.forEach(function(i){
                    promise = promise.then(function(){
                        return getEvents(i.year, null)
                        .then(function(result){
                            i.events = result.events.slice(0,3);
                            i.hasMoreEvents = result.events.length > 3;
                            i.thumbnails = result.events.reduce(function(arr, j){
                                return arr.concat(j.thumbnails);
                            }, []).slice(0, 4);
                        })
                    });
                });
                
                return promise.then(function(){
                    return {
                        timeline: arr,
                        cursor: null //arr.length >= maxItems || !cursor ? arr[arr.length-1].year : null
                    };
                });
            });
        }
        
        function getTimelineBackground()
        {
            return $q(function(resolve){
                resolve();
            })
            .then(function(){
                return {
                    photo: randomImage(1024,768),
                    credit: 'TESTING VERY VERY VERY VERY VERY VERY VERY VERY LONG CREDIT'
                };
            });
        }
        
        function getEventsSlow(year, cursor)
        {
            return $q(function(resolve){
                $timeout(resolve, 1000);
            })
            .then(function(){
                return getEvents(year, cursor);
            });
        }
        
        function getEvents(year, cursor)
        {
            return $q(function(resolve){
                resolve(angular.fromJson(angular.toJson(self.sampleEvents.filter(function(x){
                    return x.year === year;
                }))));
            })
            .then(function(arr){
                arr = arr.slice(cursor | 0, (cursor | 0) + 5);
                arr.forEach(function(event){
                    event.thumbnails = self.sampleContents.filter(function(x){
                        return x.event === event.id && x.media;
                    })
                    .slice(0, 2)
                    .map(function(x){
                        return x.media;
                    });
                });
                
                var nextYear = self.sampleEvents.map(function(x){
                    return x.year;
                }).filter(function(x){
                    return (x|0) > (year|0);
                }).sort(function(a, b){
                    return a - b;
                })[0] || null;
                
                var prevYear = self.sampleEvents.map(function(x){
                    return x.year;
                }).filter(function(x){
                    return (x|0) < (year|0);
                }).sort(function(a, b){
                    return b - a;
                })[0] || null;
                
                return {
                    events: arr,
                    nextYear: nextYear,
                    prevYear: prevYear,
                    cursor: arr.length >= 5 ? (cursor | 0) + 5 : null
                };
            });
        }
        
        function searchEventsEx(contains, cursor)
        {
            return $q.resolve()
            .then(function(){
                return self.sampleEvents.filter(function(x){
                    return x.name.toLowerCase().indexOf(contains.toLowerCase()) == 0; // simply check if name starts from given query
                });
            })
            .then(function(arr){
                arr = arr.slice(cursor | 0, (cursor | 0) + 5);
                arr = angular.fromJson(angular.toJson(arr));
                arr.forEach(function(event){
                    event.thumbnails = self.sampleContents.filter(function(x){
                        return x.event === event.id && x.media;
                    })
                    .slice(0, 2)
                    .map(function(x){
                        return x.media;
                    });
                    
                    event.html = '<strong>'+htmlEscapeSimple(event.name.slice(0, contains.length))+'</strong>'+htmlEscapeSimple(event.name.slice(contains.length))
                });
                
                return {
                    events: arr,
                    cursor: arr.length >= 5 ? (cursor | 0) + 5 : null
                };
            });
        }
        
        var html_replacements = {
           '<': '&lt;',
           '>': '&gt;',
           '"': '&quot;',
           '&': '&amp;', 
        };
        function htmlEscapeSimple(text)
        {
            return text.replace(/[<>\"&]/g, function(m){
                return html_replacements[m];
            });
        }
        
        function searchEvents(contains)
        {
            return $q(function(resolve){
                resolve();
            })
            .then(function(){
                return self.sampleEvents.filter(function(x){
                    return x.name.toLowerCase().indexOf(contains.toLowerCase()) == 0; // simply check if name starts from given query
                })
                .slice(0,4)
                .map(function(x){
                    return {
                        id: x.id,
                        name: x.name,
                        html: '<strong>'+htmlEscapeSimple(x.name.slice(0, contains.length))+'</strong>'+htmlEscapeSimple(x.name.slice(contains.length))
                    };
                });
            });
        }
        
        function getEventById(id)
        {
            return $q(function(resolve){
                resolve(self.sampleEvents.filter(function(x){
                    return x.id === id;
                }));
            })
            .then(function(arr){
               return arr.length ? arr[0] : null; 
            });
        }
        
        function getContents(event, cursor)
        {
            return $q(function(resolve){
                 resolve(angular.fromJson(angular.toJson(self.sampleContents.filter(function(x){
                     return x.event === event;
                 }))))
            })
            .then(function(arr){
                arr = arr.slice(cursor | 0, (cursor | 0) + 5);
                arr.forEach(function(content){
                   content.commentsCounter = self.sampleComments.filter(function(x){
                       return x.content === content.id;
                   }).length;
                });
                return {
                    contents: arr,
                    cursor: arr.length == 5 && (cursor | 0) + 5 || null
                };
            });
        }
        
        function getContentById(id)
        {
            return $q(function(resolve){
                resolve(self.sampleContents.filter(function(x){
                    return x.id === id;
                }));
            })
            .then(function(arr){
                arr.forEach(function(content){
                    content.commentsCounter = self.sampleComments.filter(function(x){
                        return x.content === content.id;
                    }).length;
                   
                    content.likesCounter = parseInt(Math.random()*999)
                });
                return arr.length ? arr[0] : null; 
            });
        }
        
        function toggleLikeContent(event, id)
        {
            return $q(function(resolve){
                resolve(self.sampleContents.filter(function(x){
                    return x.id === id;
                }));
            })
            .then(function(arr){
                if(arr.length != 1) return 400;
                if(arr[0].userLikedThis)
                    arr[0].likesCounter--;
                else
                    arr[0].likesCounter++;
                arr[0].userLikedThis = !arr[0].userLikedThis;
                return 200; 
            });
        }
        
        function getComments(content, notUsed, cursor)
        {
            return $q(function(resolve){
                 resolve(angular.fromJson(angular.toJson(self.sampleComments.filter(function(x){
                     return x.content === content;
                 }))))
            })
            .then(function(arr){
                arr = arr.slice(cursor | 0, (cursor | 0) + 5);
                return {
                    comments: arr,
                    cursor: arr.length == 5 && (cursor | 0) + 5 || null
                };
            });
        }
        
        function addEvent(name, year, month, day)
        {
            return $q(function(resolve){
                resolve();
            })
            .then(function(){
                var id = getNewNumber();
                self.sampleEvents.splice(0, 0, {
                    id: id,
                    year: year,
                    name: name,
                    month: month,
                    day: day
                });
                return id;
            });
        }
        
        function addContent(event, headline, details, credits, media)
        {
            return $q(function(resolve, reject){
                if(typeof(event) == 'string')
                {
                    if(self.sampleEvents.some(function(x){
                        return x.id === event;
                    }) == false)
                        return reject();
                    resolve();
                }
                else
                {
                    addEvent(event.name, event.year, event.month, event.day)
                    .then(function(id){
                        event = id;
                        resolve();
                    });
                }
            })
            .then(function(){
                var id = getNewNumber();
                if(media instanceof $window.Blob)
                {
                    media = $window.URL.createObjectURL(media);
                }
                self.sampleContents.splice(0, 0, {
                    id: id,
                    event: event,
                    headline: headline,
                    details: details,
                    credits: credits,
                    media: media && {
                        url: media,
                        type: 'video',
                        link: 'https://www.youtube.com/embed/s3qZF4jhgDo?autoplay=1&rel=0'
                    },
                    likesCounter: parseInt(Math.random()*999)
                });
                return id;
            });
        }
        
        function submitContent(event, headline, details, credits, media)
        {
            return addContent(event, headline, details, credits, media)
            .then(function(){
                return 200;
            });
        }
        
        function addComment(contentId, comment)
        {
            return $q(function(resolve, reject){
                if(self.sampleContents.some(function(x){
                    return x.id === contentId;
                }) == false)
                    return reject();
                resolve();
            })
            .then(function(){
                var id = getNewNumber();
                self.sampleComments.splice(0, 0, {
                    content: contentId,
                    message: comment
                });
                return id;
            });
        }
        
        function postComment(eventId, contentId, contentType, comment)
        {
            return addComment(contentId, comment)
            .then(function(){
                return 200;
            })
        }
        
        function getNewNumber(){
            getNewNumber.number = (getNewNumber.number|0)+1;
            return getNewNumber.number.toString();
        }
        
        function shareContentViaMail()
        {
            return $q(function(resolve){
                resolve();
            })
            .then(function(){
                return self.shareContentViaMailCode || 200;
            });
        }
        
        function reportToAdmin()
        {
            return $q(function(resolve){
                resolve();
            })
            .then(function(){
                return self.reportToAdminCode || 200;
            });
        }
        
        function randomText(minlength, maxlength)
        {
            var randomText = [
                "Of friendship on inhabiting diminution discovered as. Did friendly eat breeding building few nor. Object he barton no effect played valley afford. Period so to oppose we little seeing or branch. Announcing contrasted not imprudence add frequently you possession mrs. Period saw his","houses square and misery. Hour had held lain give yet.",
                "At as in understood an remarkably solicitude. Mean them very seen she she. Use totally written the observe pressed justice. Instantly cordially far intention recommend estimable yet her his. Ladies stairs enough esteem add fat all enable. Needed its design number winter see. Oh be","me sure wise sons no. Piqued ye of am spirit regret. Stimulated discretion impossible admiration in particular conviction up.", 
                "Boisterous he on understood attachment as entreaties ye devonshire. In mile an form snug were been sell. Hastened admitted joy nor absolute its. Extremely ham any his departure for contained curiosity defective. Way now instrument had eat diminution melancholy expression", "sentiments stimulated. One built fat you out manor books. Mrs interested now his affronting inquietude contrasted cultivated. Lasting showing expense greater on colonel no.",
                "Him rendered may attended concerns jennings reserved now. Sympathize did now preference unpleasing mrs few.",
                "Mrs for hour game room want are fond dare. For detract charmed add talking age. Shy resolution instrument unreserved man few. She did open find pain some out. If we landlord stanhill mr whatever pleasure supplied concerns so. Exquisite by it admitting cordially september newspaper","an. Acceptance middletons am it favourable. It it oh happen lovers afraid." 
            ].join(' ').split(/\s+/g);
            var length = randomText.length;
            randomText = randomText.concat(randomText);
            var start = Math.random()*length|0;
            
            var text = randomText.slice(start, Math.random()*Math.min(maxlength-minlength, length)+minlength+start).join(' ');
            return text.slice(0, 1).toUpperCase() + text.slice(1);
        }
        
        function randomImage(width, height)
        {
            width = width || 480;
            height = height || 300;
            randomImage.index = ((randomImage.index|0)+1);
            return "https://unsplash.it/"+width+"/"+height+"?image=" + randomImage.index;
        }
        
        function randomEvent(mindate, maxdate, maxcontents, maxcomments)
        {
            var minDate = new Date(mindate);
            var maxDate = new Date(maxdate);
            
            var eventDate = new Date(Math.random() * (maxDate.getTime() - minDate.getTime()) + minDate.getTime());
            
            var eventName = randomText(5, 20);
            
            addEvent(
                eventName,
                eventDate.getFullYear().toString(),
                (eventDate.getMonth()+1).toString(),
                eventDate.getDate().toString()
            )
            .then(function(event){
                var numContents = Math.random()*(maxcontents-1) + 1;
                for(var i = 0; i < numContents; i++)
                {
                    var contentHeadLine = randomText(10, 30);
                    var contentDescription = randomText(60, 100);
                    var contentCredits = randomText(40, 80);
                    addContent(
                        event,
                        contentHeadLine,
                        contentDescription,
                        contentCredits,
                        Math.random() > 0.9 ? null : randomImage()
                    )
                    .then(function(content){
                        var numComments = Math.random()*maxcomments;
                        for(var i = 0; i < numComments; i++)
                        {
                            addComment(content, randomText(2, 60));
                        }
                    });
                }
                
            })
        }
        
        (function mockupEvents(){
            var n = Math.random()*50+40;
            
            for(var i = 0; i < n; i++)
                randomEvent('2000/01/01', Date.now(), 30, 10);
        })();
    }
})();