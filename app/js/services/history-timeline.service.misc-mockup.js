(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .run(MockupMiscService);
    
    MockupMiscService.$inject = ['$q','$window','$state','$timeout','MiscService'];
    function MockupMiscService($q, $window, $state, $timeout, MiscService){
        var self = MiscService;
        if($window.localStorage.apiTestServer) return;
        $window.miscService = self;
        self.contactToAdmin = contactToAdmin;
        self.getInfo = getInfo;
        
        self.pages = {};
        
        self.pages.about = randomHTML();
        self.pages.tcs = randomHTML();
        self.pages.privacy = randomHTML();
        
        self.contactUsStatusCode = 200;
        function contactToAdmin(fullName, phoneNumber, email, topic, additionalDetails)
        {
            return $q(function(resolve){
                resolve();
            })
            .then(function(){
                return self.contactUsStatusCode;
            });
        }
        
        function getInfo(page)
        {
            return $q(function(resolve){
                resolve();
            })
            .then(function(){
                return {
                    html: self.pages[page]
                };
            });
        }
        
        function randomText(minlength, maxlength)
        {
            var randomText = [
                "Of friendship on inhabiting diminution discovered as. Did friendly eat breeding building few nor. Object he barton no effect played valley afford. Period so to oppose we little seeing or branch. Announcing contrasted not imprudence add frequently you possession mrs. Period saw his","houses square and misery. Hour had held lain give yet.",
                "At as in understood an remarkably solicitude. Mean them very seen she she. Use totally written the observe pressed justice. Instantly cordially far intention recommend estimable yet her his. Ladies stairs enough esteem add fat all enable. Needed its design number winter see. Oh be","me sure wise sons no. Piqued ye of am spirit regret. Stimulated discretion impossible admiration in particular conviction up.", 
                "Boisterous he on understood attachment as entreaties ye devonshire. In mile an form snug were been sell. Hastened admitted joy nor absolute its. Extremely ham any his departure for contained curiosity defective. Way now instrument had eat diminution melancholy expression", "sentiments stimulated. One built fat you out manor books. Mrs interested now his affronting inquietude contrasted cultivated. Lasting showing expense greater on colonel no.",
                "Him rendered may attended concerns jennings reserved now. Sympathize did now preference unpleasing mrs few.",
                "Mrs for hour game room want are fond dare. For detract charmed add talking age. Shy resolution instrument unreserved man few. She did open find pain some out. If we landlord stanhill mr whatever pleasure supplied concerns so. Exquisite by it admitting cordially september newspaper","an. Acceptance middletons am it favourable. It it oh happen lovers afraid." 
            ].join(' ').split(/\s+/g);
            var length = randomText.length;
            randomText = randomText.concat(randomText);
            var start = Math.random()*length|0;
            
            var text = randomText.slice(start, Math.random()*Math.min(maxlength-minlength, length)+minlength+start).join(' ');
            return text.slice(0, 1).toUpperCase() + text.slice(1);
        }
        
        function randomHTML(){
            var n = (Math.random()*5+5)|0;
            return '<ul>'+Array.apply(null, {length:(Math.random()*5+1)|0}).reduce(function(x){
                return x+'<li>'+randomText(30,60)+'</li>';
            },'')+'</ul>'+Array.apply(null, {length:(Math.random()*5+1)|0}).reduce(function(x){
                return x+'<p>'+randomText(30,60)+'</p>';
            },'');
        }
    }
})();