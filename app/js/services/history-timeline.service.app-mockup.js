(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .run(MockupAppService);
    
    MockupAppService.$inject = ['$q','$window','$state','$timeout','AppService'];
    function MockupAppService($q, $window, $state, $timeout, AppService){
        var self = AppService;
        if($window.localStorage.apiTestServer) return;
        $window.appService = self;
        self.getLanguages = getLanguages;
        self.getLanguageData = getLanguageData;
        self.getDomains = getDomains;
        self.getDomainById = getDomainById;
        
        self.languages = [
            {
                id: 'hebrew',
                name: 'Hebrew Hebrew Hebrew Hebrew Hebrew Hebrew Hebrew Hebrew',
                image: '/img/test-flag.jpg'
            },
            {
                id: 'english',
                name: 'English',
                image: '/img/test-flag.jpg'
            }
        ];
        
        self.domains = [
            {
                id: 'test-domain',
                name: 'Sample domain',
                logo: '/img/bg-demo.jpg',
                language: 'hebrew'
            },
            {
                id: 'test-domain-1',
                name: 'Sample domain 1',
                logo: '/img/bg-demo.jpg',
                language: 'hebrew'
            },
            {
                id: 'test-domain-2',
                name: 'Sample domain 2',
                logo: '/img/bg-demo.jpg',
                language: 'hebrew'
            },
            {
                id: 'test-domain-3',
                name: 'Sample domain 3',
                logo: '/img/bg-demo.jpg',
                language: 'hebrew'
            },
            {
                id: 'test-domain-4',
                name: 'Sample domain 4',
                logo: '/img/bg-demo.jpg',
                language: 'hebrew'
            },
            {
                id: 'test-domain-5',
                name: 'Sample domain 5',
                logo: '/img/bg-demo.jpg',
                language: 'hebrew'
            },
            {
                id: 'test-domain',
                name: 'Sample domain',
                logo: '/img/bg-demo.jpg',
                language: 'english'
            },
            {
                id: 'test-domain-2',
                name: 'Sample domain 1',
                logo: '/img/bg-demo.jpg',
                language: 'english'
            },
            {
                id: 'test-domain-3',
                name: 'Sample domain 2',
                logo: '/img/bg-demo.jpg',
                language: 'english'
            }
        ];
        
        var langTest = {
            rtl: true,
            ACCOUNT_LABEL_EMAIL: 'EMAIL',
            ACCOUNT_LABEL_PASSWORD: 'PASSWORD',
            ACCOUNT_LOGIN_PAGE_HEADER: 'LOG IN',
            ACCOUNT_LOGIN_PAGE_TITLE: 'Log in',
            ACCOUNT_BUTTON_LOGIN_FORM_SUBMIT: 'LOG IN',
            ACCOUNT_BUTTON_LOGIN: 'LOG IN',
            ACCOUNT_BUTTON_LOGIN_WITH_FACEBOOK: 'LOG IN WITH FACEBOOK',
            ACCOUNT_LABEL_SEPARATOR_OR: 'OR',
            ACCOUNT_BUTTON_REGISTER: 'REGISTER',
            ACCOUNT_BUTTON_FORGOT_PASSWORD: 'FORGOT PASSWORD',
            ACCOUNT_FORGOT_PASSWORD_PAGE_HEADER: 'FORGOT PASSWORD',
            ACCOUNT_FORGOT_PASSWORD_PAGE_TITLE: 'Forgot password',
            ACCOUNT_BUTTON_FORGOT_PASSWORD_FORM_SUBMIT: 'SEND',
            ACCOUNT_SIGNUP_PAGE_HEADER: 'SIGN UP',
            ACCOUNT_SIGNUP_PAGE_TITLE: 'SIGN UP',
            ACCOUNT_BUTTON_SIGNUP_WITH_FACEBOOK: 'SIGN UP WITH FACEBOOK',
            ACCOUNT_LABEL_USER_NAME: 'NAME',
            ACCOUNT_LABEL_CONFIRM_PASSWORD: 'CONFIRM PASSWORD',
            ACCOUNT_BUTTON_SIGNUP_FORM_SUBMIT: 'SIGN UP',
            ACCOUNT_LABEL_QUESTION_ALREADY_REGISTERED: 'ALREADY REGISTERED?',
            ACCOUNT_LABEL_QUESTION_NEW_TO_APP: 'NEW IN LOCAL STORY?',
            ENTRANCE_LABEL_REMEMBER_ME: 'REMEMBER ME',
            ENTRANCE_BUTTON_SELECT: 'SELECT',
            ENTRANCE_DOMAIN_SELECTOR_LABEL: '--Choose Local Story--',
            ACCOUNT_MSG_UNKNOWN_ERROR_TITLE: 'UNKNOWN ERROR',
            ACCOUNT_MSG_UNKNOWN_ERROR_TEXT: 'Status: ',
            ACCOUNT_MSG_AWAITING_APPROVAL_FROM_FACEBOOK_TITLE: 'AWAITING APPROVAL FROM FACEBOOK',
            ACCOUNT_MSG_AWAITING_APPROVAL_FROM_FACEBOOK_TEXT: '...',
            ACCOUNT_MSG_LOADING_TITLE: 'PLEASE WAIT...',
            ACCOUNT_MSG_LOADING_TEXT: '',
            ACCOUNT_MSG_REGISTRATION_SUCCESS_TITLE: 'REGISTRATION SUCCEEDED',
            ACCOUNT_MSG_REGISTRATION_SUCCESS_TEXT: 'We sent activation link to your email.',
            ACCOUNT_MSG_REGISTRATION_FAIL_TITLE: 'FAILED TO REGISTER',
            ACCOUNT_MSG_REGISTRATION_FAIL_TEXT: 'Email already registered!',
            ACCOUNT_MSG_LOGIN_FAIL_TITLE: 'FAILED TO LOGIN',
            ACCOUNT_MSG_LOGIN_FAIL_TEXT: 'Invalid email or password!',
            ACCOUNT_MSG_RESTORE_PASSWORD_SUCCESS_TITLE: 'SENT',
            ACCOUNT_MSG_RESTORE_PASSWORD_SUCCESS_TEXT: 'We sent restore instructions to your email.',
            ACCOUNT_MSG_RESTORE_PASSWORD_FAIL_TITLE: 'FAILED TO SEND RESTORE PASSWORD INSTRUCTIONS',
            ACCOUNT_MSG_RESTORE_PASSWORD_FAIL_TEXT: 'Email is not registered!',
            BUTTON_CANCEL: 'CANCEL',
            BUTTON_CLOSE: 'CLOSE',
            APPLICATION_NAME: 'History Timeline',
            MENU_BUTTON_BACK_TO_TIMELINE: 'BACK TO TIMELINE',
            MENU_BUTTON_ABOUT_US: 'ABOUT US',
            MENU_BUTTON_PRIVACY: 'PRIVACY POLICY',
            MENU_BUTTON_TCS: 'TERMS OF USE',
            MENU_BUTTON_CONTACT_US: 'CONTACT US',
            MENU_BUTTON_LOGOUT: 'LOG OUT',
            HOME_BUTTON_ADD_CONTENT: 'ADD CONTENT',
            HOME_LABEL_SEARCH_EVENT: 'SEARCH EVENT',
            HOME_BUTTON_SHOW_MORE_EVENTS: 'SHOW MORE',
            HOME_LABEL_NO_EVENTS: 'NO EVENTS YET',
            EVENT_SELECTOR_BUTTON_NEW_EVENT: '-NEW EVENT-',
            EVENT_SELECTOR_LABEL_NO_EVENTS_FOUND: '-NO EVENTS FOUND-',
            YEAR_PAGE_BUTTON_HOME: 'HOME',
            YEAR_PAGE_TITLE_EVENTS_IN: 'Events in',
            YEAR_PAGE_NO_EVENTS_FOUND: 'NO EVENTS FOUND',
            SEARCH_EVENTS_PAGE_TITLE_SEARCH_FOR: 'Search for',
            CONTENT_PAGE_BUTTON_ADD_CONTENT: 'ADD CONTENT',
            CONTENT_PAGE_LABEL_DETAILS: 'DETAILS',
            CONTENT_PAGE_LABEL_CREDITS: 'CREDITS',
            CONTENT_PAGE_LABEL_COMMENTS: 'COMMENTS',
            ADD_CONTENT_PAGE_TITLE: 'Add to timeline',
            ADD_CONTENT_PAGE_LABEL_ADD_CONTENT: 'ADD CONTENT',
            ADD_CONTENT_PAGE_LABEL_TCS: 'Terms and Conditions',
            ADD_CONTENT_PAGE_LABEL_ACCEPT: 'Accept',
            ADD_CONTENT_PAGE_LABEL_DETAILS: 'DETAILS',
            ADD_CONTENT_PAGE_LABEL_CREDITS: 'CREDITS',
            ADD_CONTENT_PAGE_BUTTON_FORM_SUBMIT: 'ADD TO TIMELINE',
            ADD_CONTENT_PAGE_MSG_SUCCESS_TITLE: 'SUCCESSFULLY ADDED!',
            ADD_CONTENT_PAGE_MSG_SUCCESS_TEXT: 'Content is added and it will appear on website if administration approve it.',
            ADD_CONTENT_PAGE_MSG_FAIL_TITLE: 'FAILED TO ADD CONTENT!',
            ADD_CONTENT_PAGE_MSG_FAIL_TEXT: '',
            ADD_CONTENT_PAGE_MSG_UKNOWN_ERROR_TITLE: 'FAILED TO ADD CONTENT!',
            ADD_CONTENT_PAGE_MSG_UKNOWN_ERROR_TEXT: 'Unexpected error, status: ',
            ADD_CONTENT_PAGE_MSG_UPLOADING_TITLE: 'UPLOADING...',
            ADD_CONTENT_PAGE_MSG_UPLOADING_TEXT: '',
            COMMENTS_BUTTON_POST_COMMENT: 'POST COMMENT',
            COOMENTS_LABEL_ADD_COMMENT: 'ADD COMMENT...',
            COMMENTS_BUTTON_SHOW_MORE: 'Show more',
            CONTENT_PAGE_COMMENTS_MSG_POST_COMMENT_SUCCESS_TITLE: 'DONE!',
            CONTENT_PAGE_COMMENTS_MSG_POST_COMMENT_SUCCESS_TEXT: 'Comment will be displayed when administration approve it...',
            CONTENT_PAGE_COMMENTS_MSG_POST_COMMENT_UNKNOWN_ERROR_TITLE: 'FAILED TO POST A COMMENT!',
            CONTENT_PAGE_COMMENTS_MSG_POST_COMMENT_UNKNWON_ERROR_TEXT: 'Status: ',
            CONTACT_PAGE_HEADER: 'CONTACT US',
            CONTACT_PAGE_TITLE: 'Contact us',
            CONTACT_PAGE_LABEL_FORM: 'FORM LABEL',
            CONTACT_PAGE_LABEL_FULL_NAME: 'FULL NAME',
            CONTACT_PAGE_LABEL_EMAIL: 'EMAIL',
            CONTACT_PAGE_LABEL_PHONE_NUMBER: 'PHONE NUMBER',
            CONTACT_PAGE_LABEL_TOPIC: 'TOPIC',
            CONTACT_PAGE_LABEL_ADDITIONAL_DETAILS: 'ADDITIONAL DETAILS',
            CONTACT_PAGE_BUTTON_SUBMIT_FORM: 'SEND',
            CONTACT_PAGE_LABEL_READ: 'READ',
            CONTACT_PAGE_LABEL_ABOUT_US: 'ABOUT US',
            CONTACT_PAGE_MSG_SEND_SUCCESS_TITLE: 'SENDED!',
            CONTACT_PAGE_MSG_SEND_SUCCESS_TEXT: "We'll contact you as soon as possible...",
            CONTACT_PAGE_MSG_SEND_FAIL_TITLE: 'FAILED TO SEND!',
            CONTACT_PAGE_MSG_SEND_FAIL_TEXT: 'Please check your form and try again...',
            CONTACT_PAGE_MSG_SENDING_PROGRESS_TITLE: 'PLEASE WAIT...',
            CONTACT_PAGE_MSG_SENDING_PROGRESS_TEXT: '',
            STATIC_PAGE_ABOUT_HEADER: 'ABOUT US',
            STATIC_PAGE_ABOUT_TITLE: 'About us',
            STATIC_PAGE_TCS_HEADER: 'TERMS OF USE',
            STATIC_PAGE_TCS_TITLE: 'Terms of use',
            STATIC_PAGE_PRIVACY_HEADER: 'PRIVACY POLICY',
            STATIC_PAGE_PRIVACY_TITLE: 'Privacy policy',
            REPORT_PAGE_HEADER: 'REPORT TO ADMIN',
            REPORT_PAGE_TITLE: 'Report to admin',
            REPORT_PAGE_LABEL_TOPIC: 'TOPIC',
            REPORT_PAGE_LABEL_MESSAGE: 'MESSAGE',
            REPORT_PAGE_BUTTON_SUBMIT_FORM: 'SEND',
            REPORT_PAGE_MSG_SUCCESS_TITLE: 'DONE!',
            REPORT_PAGE_MSG_SUCCESS_TEXT: 'Report successfully sended...',
            REPORT_PAGE_MSG_FAIL_TITLE: 'OOPS!',
            REPORT_PAGE_MSG_FAIL_TEXT: 'Unable to send report, try again later...',
            REPORT_PAGE_MSG_UKNOWN_ERROR_TITLE: 'OOPS!',
            REPORT_PAGE_MSG_UKNOWN_ERROR_TEXT: 'Unexpected error, status: ',
            SELECT_EVENT_PAGE_HEADER: 'SELECT EVENT',
            SELECT_EVENT_PAGE_TITLE: 'Select Event',
            SELECT_EVENT_LABEL_DETAILS: 'EVENT DETAILS',
            SELECT_EVENT_LABEL_EVENT_NAME: 'EVENT NAME',
            SELECT_EVENT_LABEL_DAY: 'DAY',
            SELECT_EVENT_LABEL_MONTH: 'MONTH',
            SELECT_EVENT_LABEL_YEAR: 'YEAR',
            SELECT_EVENT_BUTTON_FORM_SUBMIT: 'ADD TO TIMELINE',
            SHARE_VIA_EMAIL_PAGE_HEADER: 'SHARE VIA MAIL',
            SHARE_VIA_EMAIL_LABEL_RECIPIENT_NAME: 'NAME',
            SHARE_VIA_EMAIL_LABEL_RECIPIENT_EMAIL: 'EMAIL',
            SHARE_VIA_EMAIL_PLACEHOLDER_RECIPIENT_NAME: 'RECIPIENT NAME',
            SHARE_VIA_EMAIL_PLACEHOLDER_RECIPIENT_EMAIL: 'RECIPIENT EMAIL',
            SHARE_VIA_EMAIL_LABEL_MESSAGE: 'MESSAGE',
            SHARE_VIA_EMAIL_BUTTON_SUBMIT_FORM: 'SEND',
            SHARE_VIA_EMAIL_MSG_SUCCESS_TITLE: 'DONE!',
            SHARE_VIA_EMAIL_MSG_SUCCESS_TEXT: 'Content is successfully delivered to recipient...',
            SHARE_VIA_EMAIL_MSG_FAIL_TITLE: 'OOPS!',
            SHARE_VIA_EMAIL_MSG_FAIL_TEXT: "Unable to deliver the content, make sure recipient's email address is correct...",
            SHARE_VIA_EMAIL_MSG_UKNOWN_ERROR_TITLE: 'OOPS!',
            SHARE_VIA_EMAIL_MSG_UKNOWN_ERROR_TEXT: 'Unexpected error, status: ',
            CAMERA_MSG_ERROR_INVALID_FORMAT_TITLE: 'INVALID FORMAT!',
            CAMERA_MSG_ERROR_INVALID_FORMAT_TEXT: 'Only jpeg and mp4 are supported!',
            CAMERA_MSG_ERROR_VIDEO_SIZE_LIMIT_TITLE: 'VIDEO IS TOO LARGE!',
            CAMERA_MSG_ERROR_VIDEO_SIZE_LIMIT_TEXT: 'It should not be longer than 3 minutes and it should not be larger than 500 MB!',
            ERROR_PAGE_404: 'Page not found!',
            ERROR_PAGE_500: 'Internal Server Error!',
        };
        
        self.languageData = {
            'hebrew': langTest,
            'english': {
                rtl: false,
                ENTRANCE_LABEL_REMEMBER_ME: "Запам'ятати мене",
                ENTRANCE_BUTTON_SELECT: 'Вибрати',
                APPLICATION_NAME: 'Історія часу',
                ENTRANCE_DOMAIN_SELECTOR_LABEL: '--Виберіть історію--'
            }
        };
        
        function getLanguages()
        {
            return $q(function(resolve){
                resolve(JSON.parse(JSON.stringify(self.languages)));
            });
        }
        
        function getLanguageData(langId)
        {
            return $q.resolve(self.languageData[langId]);
        }
        
        function getDomains(langId)
        {
            return $q(function(resolve){
                resolve(JSON.parse(JSON.stringify(self.domains)).filter(function(x){
                    return x.language === langId;
                }));
            });
        }
        
        function getDomainById(langId, domainId)
        {
            return $q(function(resolve){
                resolve(JSON.parse(JSON.stringify(self.domains)).filter(function(x){
                    return x.language === langId && x.id === domainId;
                }));
            })
            .then(function(arr){
                if(arr.length == 0) return $q.reject();
                return arr[0];
            });
        }
    }
})();