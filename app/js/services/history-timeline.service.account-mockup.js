(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .run(MockupAccountService);
    
    MockupAccountService.$inject = ['$q','$window','$state','$timeout','AccountService','Facebook'];
    function MockupAccountService($q, $window, $state, $timeout, AccountService, Facebook){
        var self = AccountService;
        if($window.localStorage.apiTestServer) return;
        $window.accountService = self;
        
        self.login = login;
        self.logout = logout;
        self.signup = signup;
        self.signupFb = signupFb;
        self.loginFb = loginFb;
        self.restorePassword = restorePassword;
        self.hasAuthorisationCookie = hasAuthorisationCookie;
        
        self.loginStatusCode = 200;
        self.signupStatusCode = 200;
        self.signUpFbStatusCode = 200;
        self.logoutStatusCode = 200;
        self.restorePasswordStatusCode = 200;
        self.hasAuthorisationCookieValue = false;
        
        function login(email, password)
        {
            return $q(function(resolve, reject){
                $timeout(function(){
                    resolve(email.toLowerCase() === 'test@example.com' && password === '1234' ? self.loginStatusCode : 400);
                }, 1000);
            });
        }
        
        function signup(name, email, password)
        {
            return $q(function(resolve, reject){
                $timeout(function(){
                    resolve(self.signupStatusCode);
                }, 1000);
            });
        }
        
        function loginFb()
        {
            return $q(function(resolve){
                Facebook.login(function(response) {
                    resolve(response);
                });
            })
            .then(function(response){
                if(response.status === 'connected')
                    return $q(function(resolve, reject){
                        $window.localStorage[Config.AccountAuthorisedLocalStorageKey] = '1';
                        resolve(self.signUpFbStatusCode);
                    });
                console.log(response);
            });
        }
        
        function signupFb()
        {
            return loginFb();
        }
        
        function logout()
        {
            return $q(function(resolve){
                resolve(self.logoutStatusCode)
            });
        }
        
        function restorePassword(email)
        {
            return $q(function(resolve){
                $timeout(function(){
                    resolve(email.toLowerCase() === 'test@example.com' ? self.restorePasswordStatusCode : 400)
                }, 1000);
            });
        }
        
        function hasAuthorisationCookie()
        {
            return $q.resolve(!!self.hasAuthorisationCookieValue);
        }
    }
})();