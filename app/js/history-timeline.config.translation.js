(function(){
    'use strict';
    
    angular
    .module('history-timeline')
    .factory('AppMultiLangLoader', MultiLangLoaderFactory)
    .factory('AppMultiLangErrorHanlder', TranslationErrorHandlerFactory)
    .config(TranslationConfig)
    .run(TranslationUpdate);
    
    TranslationConfig.$inject = ['$translateProvider'];
    function TranslationConfig($translateProvider)
    {
        $translateProvider.useLoader('AppMultiLangLoader');
        $translateProvider.useSanitizeValueStrategy('escape');
        $translateProvider.useMissingTranslationHandler('AppMultiLangErrorHanlder');
    }
    
    MultiLangLoaderFactory.$inject = ['$q', '$state', 'AppService'];
    function MultiLangLoaderFactory($q, $state, AppService) {
        return function (options) {
            console.log('loading language', options.key);
            return AppService.getLanguageData(options.key)
            .then(function(translationData){
                if(translationData && typeof(translationData.rtl) == 'boolean'){
                    AppService.translationCache = AppService.translationCache || {};
                    AppService.translationCache[options.key] = translationData;
                    return $q.resolve(translationData);
                }
                return $q.reject(options.key);
            });
        };
    }

    function TranslationErrorHandlerFactory()
    {
        return function (translationID, uses) {
            return translationID;
        };
    }
    
    TranslationUpdate.$inject = ['$rootScope', '$state', '$translate', 'AppService']
    function TranslationUpdate($rootScope, $state, $translate, AppService)
    {
        $rootScope.$on('$translateChangeSuccess', function ($event, result) {
            $state.ApplicationName = $translate.instant('APPLICATION_NAME');
            $state.ApplicationDirectionRtL = AppService.translationCache[result.language].rtl;
        });
    }
})();