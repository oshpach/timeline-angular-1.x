angular
.module("history-timeline", [
    "ui.bootstrap",
    "ui.router",
    "ngAnimate",
    "ngTouch",
    "ngCookies",
    "facebook",
    "pascalprecht.translate",
    "dibari.angular-ellipsis"
]);
