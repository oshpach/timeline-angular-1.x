(function(){
    'use strict';
    
    angular
    .module('history-timeline')
    .factory('HttpInterceptor', HttpInterceptorService)
    .config(HttpInjector);
    
    HttpInterceptorService.$inject = ['$q', '$window', '$rootScope', 'Config'];
    function HttpInterceptorService($q, $window, $rootScope, Config) {
        return {
            // --- Response interceptor for handling errors generically ---
            responseError: function (rejection) {
                var $state = $rootScope.$state;
                console.log('intercepted http code:', rejection.status);
                if(rejection.status == 401)
                {
                    delete $window.localStorage[Config.AppStorageConfigPrefix+'AUTHORISED'];
                    $state.go('entrance');
                }
                else
                if(rejection.status == 403)
                {
                    $state.go('error', {
                        errCode: rejection.status
                    });
                }
                else
                if(rejection.status == 404)
                {
                    $state.go('error', {
                        errCode: rejection.status
                    });
                }
                else
                if(rejection.status >= 500)
                {
                    $state.go('error', {
                        errCode: rejection.status
                    });
                }
                return $q.reject(rejection);
            }
        };
    }
    
    HttpInjector.$inject = ['$provide', '$httpProvider'];
    function HttpInjector($provide, $httpProvider) {
        Decorator.$inject = ['$delegate', '$window', '$injector', 'Config'];
        $httpProvider.interceptors.push('HttpInterceptor');
        $provide.decorator('$http', Decorator);
        function Decorator($delegate, $window, $injector, AppConfig) {
            var newHttp = function $httpWrapper(config) {
                config.url = setup(config.url, config);
                return $delegate(config);
            };
            
            newHttp.get = decorateRegularCall('get');
            newHttp.delete = decorateRegularCall('delete');
            newHttp.head = decorateRegularCall('head');
            newHttp.jsonp = decorateRegularCall('jsonp');
            newHttp.post = decorateDataCall('post');
            newHttp.put = decorateDataCall('put');
            
            copyNotOverriddenAttributes(newHttp);
            
            return newHttp;
            
            function decorateRegularCall(method) {
                return function (url, config) {
                    config = config || {};
                    url = setup(url, config);
                    return $delegate[method](url, config);
                };
            }
            
            function decorateDataCall(method) {
                return function (url, data, config) {
                    config = config || {};
                    url = setup(url, config);
                    return $delegate[method](url, data, config);
                };
            }

            function copyNotOverriddenAttributes(newHttp) {
                for (var attr in $delegate) {
                    if (!newHttp.hasOwnProperty(attr)) {
                        if (typeof ($delegate[attr]) === 'function') {
                            newHttp[attr] = function () {
                                return $delegate.apply($delegate, arguments);
                            };
                        } else {
                            newHttp[attr] = $delegate[attr];
                        }
                    }
                }
            }
            
            function setup(url, config)
            {
                var appService = $injector.get('AppService');
                
                if(typeof(url) == 'object' && url && url.ApiUrl) {
                    var apiResource = url;
                    url = apiResource.ApiUrl;
                    if(AppConfig.ApiServer)
                    {
                        if(typeof(AppConfig.ApiServer) == 'string')
                            url = AppConfig.ApiServer + url;
                        else if(AppConfig.ApiServer.Domain)
                        {
                            var port = AppConfig.ApiServer.Port ? ':' + AppConfig.ApiServer.Port : '';
                            var subDomain = AppConfig.AppDomainAsApiSubDomain && apiResource.DomainSensitive && appService.currentDomain && appService.currentDomain.domainId + '.' || 'lsapi.';
                            url = AppConfig.ApiServer.Protocol + '//' + subDomain + AppConfig.ApiServer.Domain + port + url;
                        }
                    }
                    config.withCredentials = true;
                    config.headers = config.headers || {};
                    config.headers['app-language'] = appService.currentDomain && appService.currentDomain.langId;
                    config.headers['app-domain'] = appService.currentDomain && appService.currentDomain.domainId;
                }
                
                return url;
            }
        }
    }
})();