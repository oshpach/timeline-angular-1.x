(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .directive('appEventSelector', AppEventSelectorDirective)
    .directive('appEventSelectorItem', AppEventSelectorItemDirective);
    
    function AppEventSelectorItemDirective()
    {
        return {
            restrict: 'E',
            template: [
                '<li class="dropdown-item">',
                     '<button type="button" ng-click="click()">',
                         '<span ng-transclude></span>',
                     '</button>',
                 '</li>'
            ].join(''),
            transclude: true,
            replace: true,
            link: link
        };
        
        function link(scope, element, attrs)
        {
            scope.click = function(){
                scope.$eval(attrs.ngClick);
                
                for(var i = scope; i; i = i.$parent) // ng-transclude element isolates scope
                    if(i.removeList) {
                        i.removeList();
                        break;
                    }
            };
        }
    }
    
    AppEventSelectorDirective.$inject = ['$timeout', '$sce', 'TimelineService'];
    function AppEventSelectorDirective($timeout, $sce, TimelineService)
    {
        return {
            restrict: 'E',
            require: 'ngModel',
            template: [
                '<div class="input-field app-event-selector" ng-class="{\'search-icon\': value == null, required: required}" app-rtl ng-init="eventSelectorId = $id">',
                    '<input type="text" class="form-control" ng-model="eventName" ng-change="onChange()" ng-keydown="$event.which == 13 && $event.preventDefault()" ng-keyup="$event.which == 13 && onHitEnter()" ng-blur="onBlur()" ng-focus="onFocus()" placeholder="{{placeholder}}" />',
                    '<ul id="event-selector-{{eventSelectorId}}" class="dropdown search-results" ng-if="results">',
                        '<li class="dropdown-item" ng-repeat="item in results" app-rtl>',
                            '<a ui-sref="contents({event: item.id})" ng-click="preventEventSelection == null && ($event.preventDefault()||true) && selectEvent(item)">',
                                '<span ng-bind-html="item.html" class="responsive-font-0-5"></span>',
                            '</a>',
                        '</li>',
                        '<li class="dropdown-item" ng-if="allowNewEvent" app-rtl>',
                            '<button type="button" ng-click="createEvent({newEvent:true, name: eventName})">',
                                '<span class="responsive-font-0-5">{{\'EVENT_SELECTOR_BUTTON_NEW_EVENT\'|translate}}</span>',
                            '</button>',
                        '</li>',
                    '</ul>',
                '</div>'
            ].join(''),
            scope: {
               ngModel:'=',
               placeholder:'@',
               preventEventSelection:'@'
            },
            replace: true,
            link: link
        };
        
        function link(scope, element, attrs, ngModelController)
        {
            //console.log('ngModelController', ngModelController);
            //window.ngModelController = ngModelController;
            
            var inputElement = element[0].querySelector('input');
            scope.eventName = '';
            scope.$parent.setupIsolatedScope(scope);
            scope.allowNewEvent = attrs.hasOwnProperty('allowNewEvent');
            scope.required = attrs.hasOwnProperty('elementRequired');
            scope.$parent.setupIsolatedScope(scope);
            
            ngModelController.$render = function(){
                scope.value = ngModelController.$viewValue;
                
                if(scope.required)
                    ngModelController.$setValidity('required', !!scope.value);
            };
            
            if(scope.required)
                ngModelController.$setValidity('required', !!scope.value);
            
            scope.onChange = onChange;
            scope.onHitEnter = onHitEnter;
            scope.onBlur = onBlur;
            scope.onFocus = onFocus;
            scope.search = search;
            scope.selectEvent = selectEvent;
            scope.createEvent = createEvent;
            scope.pendingSearch = null;
            scope.removeList = removeList;
            scope.focus = focus;
            
            if(attrs.onInit)
                scope.$parent.$eval(attrs.onInit, {
                    $value: scope.eventName,
                    $search: scope.search,
                    $clear: scope.removeList,
                    $focus: scope.focus
                });
            
            function removeList()
            {
                scope.results = null;
            }
            
            function search(delay)
            {
                scope.selectEvent(null);
                scope.results = null;
                
                if(scope.pendingSearch != null)
                    $timeout.cancel(scope.pendingSearch);
                
                if(attrs.noSearchByEmptyQuery != null && !scope.eventName.trim())
                    return;
                
                scope.pendingSearch = $timeout(function(){
                    scope.pendingSearch = null;
                    TimelineService
                    .searchEvents(scope.eventName)
                    .then(function(results){
                        scope.results = results || null;
                        if(angular.isArray(scope.results))
                        {
                            scope.results.forEach(function(item){
                                item.html = $sce.trustAsHtml(item.html);
                            });
                        }
                    }); 
                }, delay != null ? delay : 500);
                
            }
            
            function onChange()
            {
                if(attrs.onInternalChange)
                    scope.$parent.$eval(attrs.onInternalChange, {
                        $value: scope.eventName,
                        $search: scope.search,
                        $clear: scope.removeList,
                        $focus: scope.focus
                    });
                if(attrs.autocomplete !== 'false' && attrs.autocomplete !== '0')
                    search();
            }
            
            function onHitEnter()
            {
                if(attrs.onHitEnter)
                    scope.$parent.$eval(attrs.onHitEnter, {
                        $value: scope.eventName,
                        $search: scope.search,
                        $clear: scope.removeList,
                        $focus: scope.focus
                    });
            }
            
            function onBlur()
            {
                if(attrs.onBlur)
                    $timeout(function(){
                        scope.$parent.$eval(attrs.onBlur,  {
                            $value: scope.eventName,
                            $search: scope.search,
                            $clear: scope.removeList,
                            $focus: scope.focus
                        });
                    },500);
            }
            
            function onFocus()
            {
                if(attrs.onFocus)
                    scope.$parent.$eval(attrs.onFocus,  {
                        $value: scope.eventName,
                        $search: scope.search,
                        $clear: scope.removeList
                    });
            }
            
            function focus()
            {
                $timeout(function(){
                    inputElement.focus();    
                },500)
            }
            
            function selectEvent(item)
            {
                scope.value = item;
                ngModelController.$setViewValue(scope.value ? scope.value.id : null);
                if(scope.required)
                    ngModelController.$setValidity('required', !!scope.value);
                if(item) {
                    scope.eventName = item.name;
                    scope.results = null;
                }
            }
            
            function createEvent(eventDetails)
            {
                scope.eventName = '-NEW EVENT-';
                scope.value = eventDetails;
                scope.results = null;
                ngModelController.$setViewValue(scope.value);
                if(attrs.hasOwnProperty('elementRequired'))
                    ngModelController.$setValidity('required', !!scope.value);
            }
        }
    }
})();
