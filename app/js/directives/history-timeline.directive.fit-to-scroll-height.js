(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .directive('appFitToScrollHeight', FitToScrollHeightDirective);
    
    FitToScrollHeightDirective.$inject = ['$interval'];
    function FitToScrollHeightDirective($interval)
    {
        var config = {
            restrict: 'A',
            link: link
        };
        
        function link(scope, element, attrs)
        {
            var timer = $interval(function(){
                element.css('height', '0');
                element.css('height', element[0].scrollHeight + 'px');
            }, 1000/30);
            
            scope.$on('$destroy', function(){
                $interval.cancel(timer);
            });
        }
        
        return config;
    }
})();