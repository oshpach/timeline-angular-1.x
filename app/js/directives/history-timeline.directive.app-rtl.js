(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .directive('appRtl', AppRtlDirective);
    
    AppRtlDirective.$inject = ['$state'];
    function AppRtlDirective($state)
    {
        return {
            restrict: 'A',
            link: link
        };
        
        function link(scope, element, attrs)
        {
            updateRtLState($state.ApplicationDirectionRtL);
            scope.$on('$translateChangeSuccess', function ($event, result) {
                updateRtLState($state.ApplicationDirectionRtL);
            });
            
            function updateRtLState(b)
            {
                var extraClasses = attrs.appRtl ? ' ' + attrs.appRtl : ''; 
                element.toggleClass('app-rtl'+extraClasses, !!b);
            }
        }
    }
})();
