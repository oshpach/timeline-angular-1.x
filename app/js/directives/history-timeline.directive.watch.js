(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .directive('watch', WatchDirective);
    
    function WatchDirective()
    {
        return {
            restrict: 'A',
            link: link
        };
        
        function link(scope, element, attrs)
        {
            var watcher = scope.$watch(attrs.watch, function(val){
                if(attrs.watchChange != null)
                {
                    scope.$eval(attrs.watchChange, {
                        $value: val
                    });
                }
            });
        }
    }
})();
