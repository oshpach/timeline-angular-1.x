(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .directive('showModal', ModalDialogDirective);
    
    ModalDialogDirective.$inject = ['$compile','$window'];
    function ModalDialogDirective($compile,$window)
    {
        return {
            restrict: 'A',
            link: link
        };
        
        function link(scope, element, attrs)
        {
            element.on(attrs.activateOn || 'click', activate);
            
            scope.$on('$destroy', function(){
                element.off(attrs.activateOn || 'click', activate);
            });
            
            if(attrs.hasOwnProperty('modalInit')){
                scope.$eval(attrs.modalInit || 'activateModal = $activateModal', {
                    $activateModal: activate
                });
            }
            
            
            function activate($event)
            {
                if(attrs.showModal == null) throw new Error('ShowModal(): name is not set!');
                
                var modalScope = scope.$new();
                                    
                modalScope.modalDialogs.push(modalScope);
                modalScope.application.modalStack++;
                modalScope.application.currentPage = Object.create(modalScope.application.currentPage);
                var curStack = modalScope.application.modalStack;
                var modalAttrs = '';
                if(attrs.modalController != null)
                    modalAttrs += ' ng-controller="' + attrs.modalController  + '"';
                var htmlElement = $compile(
                    '<div class="timeline-modal"' + modalAttrs + ' ng-class="modalClasses"><div class="timeline-modal-content" ng-class="{\'hide-scrollbars\': application.modalStack != ' + curStack +'}" ng-include="\'/views/modals/\'+modal.name+\'.html\'"></div></div>'    
                )(modalScope);
                
                if(attrs.modalClass != null){
                    var str = scope.$eval(attrs.modalClass);
                    if(typeof(str) == 'string')
                        htmlElement.addClass(str);
                }
                
                modalScope.$on('$destroy', function(){
                    modalScope.application.modalStack--;
                    modalScope.application.currentPage = modalScope.application.currentPage.__proto__;
                    modalScope.modalDialogs.splice(modalScope.modalDialogs.indexOf(modalScope), 1);
                    htmlElement.remove(); 
                });
                
                modalScope.modal = {};
                modalScope.modal.name = scope.$eval(attrs.showModal);
                
                modalScope.$close = function(result){
                    if(attrs.onModalClose != null){
                        scope.$eval(attrs.onModalClose, {
                            modalResult: result
                        });
                    }
                    modalScope.$destroy();
                };
                                
                angular.element($window.document.body).append(htmlElement);
                
                $event && scope.$apply();
                
                
            }
        }
    }
})();
