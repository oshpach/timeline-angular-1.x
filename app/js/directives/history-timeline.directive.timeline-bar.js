(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .directive('appTimelineBar', AppTimelineBarDirective);
    
    AppTimelineBarDirective.$inject = ['$interval', '$window', 'TimelineService'];
    function AppTimelineBarDirective($interval, $window, TimelineService)
    {
        return {
            restrict: 'E',
            require: 'ngModel',
            template: [
                '<div class="timeline-viewport clearfix" unselectable="on" ng-mousedown="$event.preventDefault(); hold($event)" ng-selectstart="$event.preventDefault()" ng-mousemove="move($event)" ng-mouseup="release($event)" ng-touchstart="$event.preventDefault(); hold($event)" ng-touchmove="move($event)" ng-touchend="release($event)">',
                    '<div class="timeline-pivot" app-timer="animateTimeline()" app-timer-delay="1000/30" app-timer-active-if="timelineIsAnimating">',
                        '<div ng-repeat="item in timeline" class="timeline-item" app-timer="currentImage = (currentImage + 1) % item.thumbnails.length; prevImage = (currentImage + item.thumbnails.length - 1) % item.thumbnails.length" app-timer-delay="2000" app-timer-active-if="currentIndex === $index" ng-if="(($index+1)+offsetPercentage/100)*itemWidth > -timelineBarWidth * 1.2 && (($index+1)+offsetPercentage/100)*itemWidth < timelineBarWidth * 1.2" ng-class="{active: currentIndex === $index}" ng-style="{right: (($index+1)*100+offsetPercentage).toFixed(3)+\'%\'}" ng-init="prevImage = 0; currentImage = 0">',
                            '<div class="timeline-image" disabled-style="background-image: {{\'url(\'+item.thumbnails[0].url+\')\'}}">',
                            '</div>',
                            '<div ng-repeat="image in item.thumbnails" class="timeline-image" ng-class="{hide: $index != prevImage}" style="background-image: {{\'url(\'+image.url+\')\'}}">',
                            '</div>',
                            '<div ng-repeat="image in item.thumbnails" class="timeline-image" ng-class="{inactive: $index != currentImage, active: $index == currentImage}" style="background-image: {{\'url(\'+image.url+\')\'}}">',
                            '</div>',
                            '<div class="timeline-year text-center">{{item.year}}</div>',
                            '<a ui-sref="events({year:item.year})" ng-click="$state.go(\'events\',{year:item.year})" ng-if="item.events.length"></a>',
                        '</div>',
                    '</div>',
                    '<div class="timeline-lock-screen" ng-class="{active: currentIndex == null && velocity != 0}"></div>',
                '</div>'
            ].join(''),
            replace: true,
            link: link
        };
        
        function link(scope, element, attrs, ngModelController)
        {
            scope.hold = hold;
            scope.move = move;
            scope.release = release;
            scope.offsetPercentage = 0;
            scope.velocity = 0;
            scope.lastX = null;
            scope.currentIndex = null;
            
            
            scope.animateTimeline = function(){
                scope.timelineIsAnimating = Math.abs(scope.velocity) >= 0.0001;
                if(!scope.itemWidth)
                {
                    var item = element[0].querySelector('.timeline-pivot');
                    if(item){
                        scope.itemWidth = parseInt(getComputedStyle(item).width);
                    }
                }
                scope.timelineBarWidth = element[0].clientWidth;
                if(Math.abs(scope.velocity) > 0.1)
                {
                    scope.currentIndex = null;
                }
                if(scope.timeline && scope.itemWidth && attrs.getMoreData && scope.timeline.length * scope.itemWidth - scope.offsetPercentage * scope.itemWidth * -0.01 < element[0].clientWidth)
                {
                    scope.$eval(attrs.getMoreData);
                }
                if(!scope.timeline || scope.timeline.length == 0) return;
                if(scope.lastX == null && scope.currentIndex != null && Math.abs(scope.velocity) < 0.0001) {
                    return ngModelController.$setViewValue(scope.currentIndex);
                }
                scope.offsetPercentage -= scope.velocity;
                if(scope.startX != null)
                {
                    scope.offsetPercentage = (scope.curX - scope.startX) / scope.itemWidth * -100 + scope.oldOffset;
                }
                scope.velocity *= 0.7;
                scope.offsetPercentage = Math.min(Math.max((scope.timeline.length-1)*-100, scope.offsetPercentage), 0);
                if(Math.abs(scope.velocity) < 1 && scope.lastX == null)
                {
                    var target = Math.round(scope.offsetPercentage / 100) * 100;
                    scope.velocity = (target - scope.offsetPercentage) * -0.25;
                    if(Math.abs(scope.velocity) < 0.01 && Math.abs(target - scope.offsetPercentage) < 1)
                    {
                        var targetIndex = parseInt(Math.abs(Math.round(scope.offsetPercentage / 100)));
                        scope.currentIndex = targetIndex;
                        ngModelController.$setViewValue(targetIndex);
                        return;
                    }
                }
                if(Math.abs(scope.velocity) >= 0.01)
                    ngModelController.$setViewValue(null);
                
            };
            
            angular.element($window).on('mouseup', release);
            angular.element($window).on('resize', scope.animateTimeline);
            
            var firstLoad = true;
            scope.$watch('timeline', function(curr, prev){
                if((prev == null && curr != null) || (curr != null && firstLoad))
                {
                    if(scope.startFromYear && scope.timeline.length)
                    {
                        var position = 0;
                        for(var i = 1; i < scope.timeline.length; i++)
                        {
                            if(scope.timeline[i].year < scope.startFromYear) break;
                            position = i;
                        }
                        scope.offsetPercentage = position * -100;
                        scope.currentIndex = position;
                        ngModelController.$setViewValue(position);
                    }
                    else
                    if(scope.timeline.length > 2)
                    {
                        scope.offsetPercentage = -100;
                        scope.currentIndex = 1;
                        ngModelController.$setViewValue(1);
                    }
                }
                firstLoad = false;
                scope.timelineIsAnimating = true;
            });
            
            scope.$on('$destroy', function(){
                //$interval.cancel(scope.interval);
                angular.element($window).off('mouseup', release);
                angular.element($window).off('resize', scope.animateTimeline);
            });
            
            function extractEventItems($event)
            {
                if($event.touches && $event.touches.length)
                {
                    return $event.touches[0];
                }
                return $event;
            }
            
            var interactionTime = null;
            function hold($event)
            {
                $event = extractEventItems($event);
                scope.lastX = $event.pageX;
                scope.startX = $event.pageX;
                scope.curX = $event.pageX;
                scope.oldOffset = scope.offsetPercentage;
                interactionTime = Date.now();
            }
            
            function move($event)
            {
                $event = extractEventItems($event);
                if(scope.lastX != null)
                {
                    var item = element[0].querySelector('.timeline-pivot');
                    if(item){
                        var itemWidth = parseInt(getComputedStyle(item).width);
                        //scope.velocity += ($event.pageX - scope.lastX) / (element[0].clientWidth / itemWidth);
                        scope.velocity += ($event.pageX - scope.lastX) / itemWidth * 100;
                        scope.itemWidth = itemWidth;
                    }
                    scope.lastX = $event.pageX;
                    scope.timelineIsAnimating = true;
                }
                scope.curX = $event.pageX;
            }
            
            function release($event)
            {
                $event = extractEventItems($event);
                scope.lastX = null;
                scope.startX = null;
                if(Date.now()-interactionTime < 100 && scope.application.isMobile)
                {
                    var link = Array.apply(null, $event.path).reduce(function(anchor, x){
                        return anchor || (x.tagName === 'A' && x);
                    }, null);
                    link && $window.setTimeout(function(){
                        link.click();
                    });
                }
            }
        }
        
    }
    
})();