(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .directive('appTimer', AppTimerDirective);
    
    AppTimerDirective.$inject = ['$interval'];
    function AppTimerDirective($interval)
    {
        return {
            restrict: 'A',
            link: link
        };
        
        function link(scope, element, attrs)
        {
            scope.intervalTimer = null;
            scope.$watch(attrs.appTimerActiveIf, function(val){
                if(!val && scope.intervalTimer != null)
                {
                    $interval.cancel(scope.intervalTimer);
                    scope.intervalTimer = null;
                }
                else
                if(val && scope.intervalTimer == null)
                    scope.intervalTimer = $interval(onTimer, scope.$eval(attrs.appTimerDelay));
            });
            
            scope.$on('$destroy', function(){
                if(scope.intervalTimer != null)
                    $interval.cancel(scope.intervalTimer);
            });
            
            function onTimer()
            {
                scope.$eval(attrs.appTimer);
            }
        }
    }
})();