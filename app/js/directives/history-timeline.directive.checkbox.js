(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .directive('appCheckbox', AppCheckboxDirective);
    
    function AppCheckboxDirective()
    {
        return {
            restrict: 'E',
            require: 'ngModel',
            template: '<button id="{{objectId}}" class="app-checkbox {{classes}}" ng-class="{checked: checked}" ng-click="$event.preventDefault(); toggleClick()"></button>',
            scope: {
               ngModel:'=',
               objectId:'@'
            },
            replace: true,
            link: link
        };
        
        function link(scope, element, attrs, ngModelController)
        {
            //console.log('ngModelController', ngModelController);
            //window.ngModelController = ngModelController;
            
            scope.classes = attrs.class;
            
            ngModelController.$render = function(){
                scope.checked = !!ngModelController.$viewValue;
                
                if(attrs.hasOwnProperty('elementRequired'))
                    ngModelController.$setValidity('required', scope.checked);
            };
            
            if(attrs.hasOwnProperty('elementRequired'))
                ngModelController.$setValidity('required', scope.checked);
            
            scope.toggleClick = function(){
                scope.checked = !scope.checked;
                ngModelController.$setViewValue(scope.checked);
                if(attrs.hasOwnProperty('elementRequired'))
                    ngModelController.$setValidity('required', scope.checked);
            };
        }
    }
})();
