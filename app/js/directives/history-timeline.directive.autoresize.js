(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .directive('autoresize', AutoresizeDirective);
    
    function AutoresizeDirective()
    {
        return {
            restrict: 'A',
            link: link
        };
        
        function link(scope, element, attrs)
        {
            element.on('input', resize);
            element.on('change', resize);
            
            element.css('overflow-y', 'hidden');
            element.addClass('autoresize-animate');
            
            scope.$on('$destroy', function(){
                element.off('input', resize);
                element.off('change', resize);
            });
            
            resize();
            
            function resize()
            {
                element.css('height', 'auto');
                element.css('height', element[0].scrollHeight + 'px');
            }
        }
    }
})();
