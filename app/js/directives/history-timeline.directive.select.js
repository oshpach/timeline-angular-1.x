(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .directive('appSelect', AppSelectDirective)
    .directive('appSelectItem', AppSelectItemDirective);
    
    function AppSelectItemDirective()
    {
        return {
            restrict: 'E',
            require: '^appSelect',
            template: [
                '<li class="app-select-item" ng-mousedown="select()" ng-touchstart="select()">',
                    '<span ng-transclude>',
                    '</span>',
                '</li>'
            ].join(''),
            replace: true,
            transclude: true,
            link: link
        };
        
        function link(scope, element, attrs, directiveController)
        {
            scope.select = function(){
                directiveController.scope.value = attrs.value;
                directiveController.scope.update();
            };
        }
    }
    
    AppSelectDirective.$inject = ['$document'];
    function AppSelectDirective($document)
    {
        return {
            restrict: 'E',
            require: 'ngModel',
            template: [
                '<span class="input-field app-select show" ng-class="{required: required}" app-rtl ng-click="false && focus()">',
                    '<input type="text" placeholder="{{placeholder}}" ng-model="value" ng-required="required" readonly ng-focus="showList = true" ng-blur="showList = false"/>',
                    '<ul class="dropdown" ng-transclude ng-show="showList">',
                    '</ul>',
                '</span>'
            ].join(''),
            scope: {
               placeholder:'=' 
            },
            replace: true,
            transclude: true,
            controller: ['$scope',DirectiveController],
            link: link
        };
        
        function DirectiveController($scope)
        {
            this.scope = $scope;
        }
        
        function link(scope, element, attrs, ngModelController)
        {
            scope.required = attrs.hasOwnProperty('elementRequired');
            scope.$parent.setupIsolatedScope(scope);
            
            scope.focus = function()
            {
                element[0].querySelector('input').focus();
            };
            
            ngModelController.$render = function(){
                scope.value = ngModelController.$viewValue;
                
                if(scope.required)
                    ngModelController.$setValidity('required', !!scope.value);
            };
            
            if(scope.required)
                ngModelController.$setValidity('required', scope.required);
            
            scope.update = function(){
                ngModelController.$setViewValue(scope.value);
                if(attrs.hasOwnProperty('elementRequired'))
                    ngModelController.$setValidity('required', !!scope.value);
            };
        }
    }
    
    /*function AppSelectDirective()
    {
        return {
            restrict: 'E',
            require: 'ngModel',
            template: [
                '<span class="input-field app-select show {{classes}}" ng-class="{required: required}">',
                    '<select ng-model="value" ng-change="update()" ng-transclude>',
                    '</select>',
                '</span>'
            ].join(''),
            scope: {
               ngModel:'=' 
            },
            replace: true,
            transclude: true,
            link: link
        };
        
        function link(scope, element, attrs, ngModelController)
        {
            //console.log('ngModelController', ngModelController);
            //window.ngModelController = ngModelController;
            
            scope.classes = attrs.class || '';
            scope.required = attrs.hasOwnProperty('elementRequired'); 
            
            ngModelController.$render = function(){
                scope.value = ngModelController.$viewValue;
                
                if(scope.required)
                    ngModelController.$setValidity('required', !!scope.value);
            };
            
            if(scope.required)
                ngModelController.$setValidity('required', scope.required);
            
            scope.update = function(){
                ngModelController.$setViewValue(scope.value);
                if(attrs.hasOwnProperty('elementRequired'))
                    ngModelController.$setValidity('required', !!scope.value);
            };
        }
    }*/
})();
