
angular
.module('history-timeline')
.config(['FacebookProvider','Config',function(FacebookProvider, Config) {
    FacebookProvider.init(Config.FacebookAppId);
}]);