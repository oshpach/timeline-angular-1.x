
angular
.module('history-timeline')
.constant('Config', {
    FacebookAppId: '672662432824127',
    AppDateSelectorMaxItems: 200,
    ApiServer: {
        Domain: 'localtimeline.com', // default = self => ignores other parameters
        Protocol: 'http:',
        Port: '', // default = 80
    },
    html5location: false, // use HTML5 history API instead of location hashes
    AppDomainAsApiSubDomain: false, // if true, it will include domain id to api server domain as sub domain
    TranslationStorageKeyPrefix: 'TimelineAppTranslation_',
    AppStorageConfigPrefix: 'TimelineAppConfig_'
})
.constant('ApiUrl', {
    GetLanguages: {
        ApiUrl: "/api/get-languages"
    },
    GetLanguageData: {
        ApiUrl: "/api/get-language-data"
    },
    GetDomains: {
        ApiUrl: "/api/get-domains"
    },
    GetDomainById: {
        ApiUrl: "/api/get-domain-by-id"
    },
    SignUp: {
        ApiUrl: "/api/account/signup"
    },
    SignUpFacebook: {
        ApiUrl: "/api/account/signup-facebook"
    },
    LogIn: {
        ApiUrl: "/api/account/login"
    },
    LogOut: {
        ApiUrl: "/api/account/logout"
    },
    RestorePassword: {
        ApiUrl: "/api/account/restore-password"
    },
    Search: {
        ApiUrl: "/api/timeline/search",
        DomainSensitive: true
    },
    GetTimeline: {
        ApiUrl: "/api/timeline/get",
        DomainSensitive: true
    },
    GetBackground: {
        ApiUrl: "/api/timeline/get-background",
        DomainSensitive: true
    },
    GetStaticPage: {
        ApiUrl: "/api/get-page"
    },
    ContactToAdministration: {
        ApiUrl: "/api/contact",
        DomainSensitive: true
    },
    GetEvents: {
        ApiUrl: "/api/timeline/get-events",
        DomainSensitive: true
    },
    GetEventById: {
        ApiUrl: "/api/timeline/get-event-by-id",
        DomainSensitive: true
    },
    SearchEvents: {
        ApiUrl: "/api/timeline/search-events",
        DomainSensitive: true
    },
    GetContents: {
        ApiUrl: "/api/timeline/get-contents",
        DomainSensitive: true
    },
    GetContentById: {
        ApiUrl: "/api/timeline/get-content-by-id",
        DomainSensitive: true
    },
    ToggleLikeContent: {
        ApiUrl: "/api/timeline/toggle-like-content",
        DomainSensitive: true
    },
    GetComments: {
        ApiUrl: "/api/timeline/get-comments",
        DomainSensitive: true
    },
    AddComment: {
        ApiUrl: "/api/timeline/add-comment",
        DomainSensitive: true
    },
    AddEvent: {
        ApiUrl: "/api/timeline/add-event",
        DomainSensitive: true
    },
    AddContent: {
        ApiUrl: "/api/timeline/add-content",
        DomainSensitive: true
    },
    ReportToAdmin: {
        ApiUrl: "/api/timeline/report-to-admin",
        DomainSensitive: true
    },
    ShareViaMail: {
        ApiUrl: "/api/timeline/share-via-mail",
        DomainSensitive: true
    }
});