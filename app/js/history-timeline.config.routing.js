(function(){
    "use strict";
    
    angular
    .module("history-timeline")
    .config(RouterSetup)
    .run(RouterScopeSetup);
    
    RouterSetup.$inject = ['$stateProvider','$urlRouterProvider','$locationProvider','Config'];
    function RouterSetup($stateProvider,$urlRouterProvider,$locationProvider,Config){
        Config.html5location && $locationProvider.html5Mode(true);
        
        $urlRouterProvider
            .when('','/');
        
        $stateProvider.state('entrance',{
            url: '/',
            controller: 'EntranceScreenController',
            templateUrl: '/views/entrance/page.html',
            autoredirectIfAuthorised: true
        });
        
        $stateProvider.state('domain', {
            url: '/:langId/:domainId',
            resolve: {
                domainData: ResolveDomain
            },
            template: '<div ui-view class="app-router-view" autoscroll="true"></div>',
            abstract: true
        });
        
        $stateProvider.state('login', {
            url: '/login',
            controller: 'AccountController',
            templateUrl: '/views/account/login.html',
            pageTitle: "'ACCOUNT_LOGIN_PAGE_TITLE'|translate",
            parent: 'domain',
            anonymousUsersOnly: true,
            autoredirectIfAuthorised: true
        });
        
        $stateProvider.state('forgotPassword', {
            url: '/forgot-password',
            controller: 'AccountController',
            templateUrl: '/views/account/forgot-password.html',
            pageTitle: "'ACCOUNT_FORGOT_PASSWORD_PAGE_TITLE'|translate",
            parent: 'domain',
            anonymousUsersOnly: true,
            autoredirectIfAuthorised: true
        });
        
        $stateProvider.state('signup', {
            url: '/signup',
            controller: 'AccountController',
            templateUrl: '/views/account/signup.html',
            pageTitle: "'ACCOUNT_SIGNUP_PAGE_TITLE'|translate",
            parent: 'domain',
            anonymousUsersOnly: true,
            autoredirectIfAuthorised: true
        });
        
        $stateProvider.state('home', {
            url: '/home?year',
            templateUrl: '/views/timeline/home.html',
            controller: 'HomeController',
            pageTitle: "application.domainName",
            isHome: true,
            parent: 'domain'
        });
        
        $stateProvider.state('search', {
            url: '/search?q',
            templateUrl: '/views/timeline/search.html',
            controller: 'SearchController',
            pageTitle: "('SEARCH_EVENTS_PAGE_TITLE_SEARCH_FOR'|translate)+': '+($stateParams.q||'').trim()",
            parent: 'domain'
        });
        
        $stateProvider.state('events', {
            url: '/events?year',
            controller: 'EventsController',
            templateUrl: '/views/timeline/events.html',
            pageTitle: "('YEAR_PAGE_TITLE_EVENTS_IN'|translate) + ' ' + $stateParams.year",
            parent: 'domain'  
        });
        
        $stateProvider.state('contents', {
            url: '/contents?event',
            controller: 'ContentsController',
            templateUrl: '/views/timeline/contents.html',
            pageTitle: "application.page.event.name + ' - ' + application.page.event.year",
            parent: 'domain'
        });
        
        $stateProvider.state('content', {
            url: '/content?id&type',
            controller: 'ContentsController',
            templateUrl: '/views/timeline/contents.html',
            pageTitle: "application.page.event.name + ' - ' + application.page.event.year",
            parent: 'domain'  
        });
        
        $stateProvider.state('addToTimeline', {
            url: '/add-to-timeline?event',
            controller: 'ContentsController',
            templateUrl: '/views/timeline/add.html',
            pageTitle: "'ADD_CONTENT_PAGE_TITLE'|translate",
            parent: 'domain'  
        });
        
        $stateProvider.state('showInfo', {
            url: '/info/:page',
            templateUrl: '/views/misc/static-info.html',
            controller: 'MiscController',
            pageTitle: "('STATIC_PAGE_'+$stateParams.page.toUpperCase()+'_TITLE')|translate",
            parent: 'domain'
        });
        
        $stateProvider.state('writeMessageToAdmin', {
            url: '/contact-us',
            templateUrl: '/views/misc/contact-us.html',
            controller: 'MiscController',
            pageTitle: "'CONTACT_PAGE_TITLE'|translate",
            parent: 'domain'
        });
        
        $stateProvider.state('error', {
            url: '/{errCode:404|403|500|501|502|503|504}',
            templateUrl: '/views/misc/errors.html'
        })
        
        $urlRouterProvider.otherwise('/404');
    }
    
    RouterScopeSetup.$inject = ['$rootScope', '$state', '$stateParams', 'AppService', 'AccountService'];
    function RouterScopeSetup($rootScope, $state, $stateParams, AppService, AccountService)
    {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $state.appData = {};
        
        $rootScope.$on('$stateChangeStart', function($event,targetState){
            console.log('state starts to change to', targetState.name);
            AppService
            .loadSavedDomain()
            .then(function(){
                if(AppService.currentDomain && targetState.autoredirectIfAuthorised)
                {
                    AccountService.hasAuthorisationCookie()
                    .then(function(hasIt){
                        if(hasIt)
                            $state.go('home', {
                                domainId: AppService.currentDomain.domainId,
                                langId: AppService.currentDomain.langId
                            });
                        else
                        if(!targetState.anonymousUsersOnly)
                            $state.go('login', {
                                domainId: AppService.currentDomain.domainId,
                                langId: AppService.currentDomain.langId
                            });
                    });
                }
            })
        })
        
        $rootScope.$on('$stateChangeSuccess', function($event,newState){
            console.log('state changed to', newState.name);
        })
    }
    
    ResolveDomain.$inject = ['$q','$state', '$stateParams', '$rootScope', '$translate', 'AppService'];
    function ResolveDomain($q, $state, $stateParams, $rootScope, $translate, AppService)
    {
        return $q.resolve()
        .then(function(){
            return $translate.use($stateParams.langId);
        })
        .then(function(){
            if($state.appData.domain && $state.appData.domain.id === $stateParams.domainId) return;
            return AppService.getDomainById($stateParams.langId, $stateParams.domainId)
            .then(function success(domain){
                $state.appData.domain = domain;
            })
            .then(function(){
                return AppService.selectDomain($stateParams.langId, $stateParams.domainId);
            })
        });
    }
})();