(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .controller('CommentsController', CommentsController);
    
    CommentsController.$inject = ['$scope', '$q', '$translate', '$timeout', 'TimelineService'];
    function CommentsController($scope, $q, $translate, $timeout, TimelineService)
    {
        $scope.comments = [];
        $scope.cursor = null;
        $scope.fetchComments = fetchComments;
        $scope.hasMore = false;
        
        $scope.addComment = addComment;
        $scope.textAreaFocus = textAreaFocus;
        $scope.textAreaBlur = textAreaBlur;
        
        $scope.alert = {title:'',text:'',actionsTemplateName:''};
        
        function addComment(event, content, comment, alertDialog, showAlertDialogFn)
        {
            comment = (comment||'').trim();
            return TimelineService
            .addComment(event.id, content.id, content.media.type, comment)
            .then(function(status){
                if(status == 200)
                {
                    return $q.all([
                        $translate('CONTENT_PAGE_COMMENTS_MSG_POST_COMMENT_SUCCESS_TITLE'),
                        $translate('CONTENT_PAGE_COMMENTS_MSG_POST_COMMENT_SUCCESS_TEXT')
                    ])
                    .then(function(translation){
                        alertDialog.title = translation[0];
                        alertDialog.text = translation[1];
                        alertDialog.actionsTemplateName = 'close-all-dialogs';
                    });
                }
                else
                {
                    return $q.all([
                        $translate('CONTENT_PAGE_COMMENTS_MSG_POST_COMMENT_UNKNOWN_ERROR_TITLE'),
                        $translate('CONTENT_PAGE_COMMENTS_MSG_POST_COMMENT_UNKNWON_ERROR_TEXT')
                    ])
                    .then(function(translation){
                        alertDialog.title = translation[0];
                        alertDialog.text = translation[1] + status;
                        alertDialog.actionsTemplateName = 'close-all-dialogs';
                    });
                }
            })
            .then(function(){
                showAlertDialogFn();
            });
        }
        
        function fetchComments()
        {
            var cursor = $scope.cursor;
            $scope.cursor = null;
            return TimelineService
            .getComments($scope.contentId, $scope.contentType, cursor)
            .then(function(result){
                if(result.comments && result.comments.length)
                {
                    $scope.comments = $scope.comments.concat(result.comments);
                    $scope.cursor = result.cursor;;
                }
            });
        }
        
        function textAreaFocus()
        {
            $scope.displayPostComment = true;
        }
        
        function textAreaBlur()
        {
            $timeout(function(){
                $scope.displayPostComment = false;
                $scope.newComment = $scope.newComment.trim();
            }, 500);
            
        }
    }
})();
