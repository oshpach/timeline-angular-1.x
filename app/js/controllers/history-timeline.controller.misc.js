(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .controller('MiscController', MiscController);
    
    MiscController.$inject = ['$scope', '$q', '$state', '$stateParams', '$sce', '$translate', 'MiscService'];
    function MiscController($scope, $q, $state, $stateParams, $sce, $translate, MiscService)
    {
        $scope.getStaticInfo = getStaticInfo;
        $scope.contactToAdmin = contactToAdmin;
        
        function contactToAdmin(fullName, phoneNumber, email, topic, additionalDetails)
        {
            return $q.resolve()
            .then(function(){
                return $q.all([
                    $translate('CONTACT_PAGE_MSG_SENDING_PROGRESS_TITLE'),
                    $translate('CONTACT_PAGE_MSG_SENDING_PROGRESS_TEXT')
                ]);
            })
            .then(function(translation){
                $scope.alert = {
                    title: translation[0],
                    text: translation[1],
                    actionsTemplateName: '',
                    hideCloseDialogIcon: true
                };
                $scope.displayAlert();
            })
            .then(function(){
                return MiscService.contactToAdmin(fullName, phoneNumber, email, topic, additionalDetails);
            })
            .then(function(status){
                $scope.$closeAllModals();
                $scope.alert = {
                    actionsTemplateName: 'close-dialog'
                };
                if(status == 200)
                {
                    return $q.all([
                        $translate('CONTACT_PAGE_MSG_SEND_SUCCESS_TITLE'),
                        $translate('CONTACT_PAGE_MSG_SEND_SUCCESS_TEXT')
                    ])
                    .then(function(translation){
                        $scope.alert.title = translation[0];
                        $scope.alert.text = translation[1];
                        $scope.alert.onCloseIconClick = $scope.alert.closeBtnAction = '$state.go("home")';
                    });;
                }
                else
                {
                    return $q.all([
                        $translate('CONTACT_PAGE_MSG_SEND_FAIL_TITLE'),
                        $translate('CONTACT_PAGE_MSG_SEND_FAIL_TEXT')
                    ])
                    .then(function(translation){
                        $scope.alert.title = translation[0];
                        $scope.alert.text = translation[1];
                    });
                }
            })
            .then(function(){
                $scope.displayAlert();
            });
        }
        
        function getStaticInfo()
        {
            MiscService.getInfo($stateParams.page)
            .then(function(result){
                $scope.pageData = result;
                if($scope.pageData.html)
                    $scope.pageData.html = $sce.trustAsHtml($scope.pageData.html);
            });
        }
    }
})();