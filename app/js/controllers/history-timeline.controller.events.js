(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .controller('EventsController', EventsController);
    
    EventsController.$inject = ['$scope','$stateParams', '$window', 'TimelineService'];
    function EventsController($scope,$stateParams, $window, TimelineService)
    {
        $scope.events = null;
        
        $scope.cursor = null;
        
        $scope.getEventsList = getEventsList;
        
        var windowElement = angular.element($window)
        .on('scroll', onPageScroll)
        .on('resize', onPageScroll);
        
        $scope.$on('$destroy', function(){
            windowElement.off('scroll', onPageScroll);
            windowElement.off('resize', onPageScroll);
        });
        
        function getEventsList(){
            var cursor = $scope.cursor;
            $scope.cursor = null;
            return TimelineService
            .getEvents($stateParams.year, cursor)
            .then(function(result){
                if(angular.isArray(result.events))
                {
                    $scope.events = ($scope.events || []).concat(result.events);
                    $scope.cursor = result.cursor;
                    if($scope.yearItem == null)
                    {
                        $scope.yearItem = {
                            year: $stateParams.year
                        };
                        $scope.nextYearNumber = result.nextYear;
                        $scope.prevYearNumber = result.prevYear;
                    }
                }
            });
        }
        
        function onPageScroll(){
            if($window.innerHeight + $window.pageYOffset > $window.document.body.scrollHeight - 100)
            {
                $scope.cursor && $scope.getEventsList();
            }
        }
    }
})();
