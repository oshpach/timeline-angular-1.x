(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .controller('AccountController', AccountController);
    
    AccountController.$inject = ['$scope', '$q', '$state', '$translate', 'AccountService']
    function AccountController($scope, $q, $state, $translate, AccountService)
    {
        $scope.login = login;
        $scope.signup = signup;
        $scope.loginFb = loginFb;
        $scope.signupFb = signupFb;
        $scope.logout = logout;
        $scope.restorePassword = restorePassword;
        
        if($state.current.anonymousUsersOnly)
        {
            AccountService.hasAuthorisationCookie()
            .then(function(hasIt){
                hasIt && $state.go('home');
            });
        }
        
        function login(email, password, rememberMe)
        {
            return $q.resolve()
            .then(function(){
                return $q.all([
                    $translate('ACCOUNT_MSG_LOADING_TITLE'),
                    $translate('ACCOUNT_MSG_LOADING_TEXT')
                ])
            })
            .then(function(translation){
                $scope.alert = {
                    title: translation[0],
                    text: translation[1],
                    actionsTemplateName: '',
                    hideCloseDialogIcon: true
                };
                $scope.displayAlert();
                return AccountService
                .login(email, password, rememberMe)
            })
            .then(function (status){
                $scope.$closeAllModals();
                if(status == 200)
                {
                    $state.go('home');
                }
                else
                if(status == 400)
                {
                    // show alert
                    //  show-modal="'alert-dialog'" ng-init="alert = {title:'NOT SET',text:'NOT SET',actionsTemplateName:'',processing:false}" modal-class="'alert-dialog'" activate-on="none" modal-init="displayAlert = $activateModal"
                    return $q.all([
                        $translate('ACCOUNT_MSG_LOGIN_FAIL_TITLE'),
                        $translate('ACCOUNT_MSG_LOGIN_FAIL_TEXT')
                    ])
                    .then(function(translation){
                        $scope.alert = {
                            title: translation[0],
                            text: translation[1],
                            actionsTemplateName: 'close-dialog'
                        };
                        $scope.displayAlert();
                    });
                }
                else
                {
                    return $q.all([
                        $translate('ACCOUNT_MSG_UNKNOWN_ERROR_TITLE'),
                        $translate('ACCOUNT_MSG_UNKNOWN_ERROR_TEXT')
                    ])
                    .then(function(translation){
                        $scope.alert = {
                            title: translation[0],
                            text: translation[1] + status,
                            actionsTemplateName: 'close-dialog'
                        };
                        $scope.displayAlert();
                    });
                }
            });
        }
        
        function signup(name, email, password)
        {
            return $q.resolve()
            .then(function(){
                return $q.all([
                    $translate('ACCOUNT_MSG_LOADING_TITLE'),
                    $translate('ACCOUNT_MSG_LOADING_TEXT')
                ])
            })
            .then(function(translation){
                $scope.alert = {
                    title: translation[0],
                    text: translation[1],
                    actionsTemplateName: '',
                    hideCloseDialogIcon: true
                };
                $scope.displayAlert();
                return AccountService
                .signup(name, email, password)
            })
            .then(function (status){
                $scope.$closeAllModals();
                if(status == 200)
                {
                    // show success alert
                    return $q.all([
                        $translate('ACCOUNT_MSG_REGISTRATION_SUCCESS_TITLE'),
                        $translate('ACCOUNT_MSG_REGISTRATION_SUCCESS_TEXT')
                    ])
                    .then(function(translation){
                        $scope.alert = {
                            title: translation[0],
                            text: translation[1],
                            actionsTemplateName: 'close-dialog'
                        };
                        $scope.alert.onCloseIconClick = $scope.alert.closeBtnAction = "$state.go('login')";
                    });
                }
                else
                if(status == 400)
                {
                    // show error alert
                    return $q.all([
                        $translate('ACCOUNT_MSG_REGISTRATION_FAIL_TITLE'),
                        $translate('ACCOUNT_MSG_REGISTRATION_FAIL_TEXT')
                    ])
                    .then(function(translation){
                        $scope.alert = {
                            title: translation[0],
                            text: translation[1],
                            actionsTemplateName: 'close-dialog'
                        };
                    });
                }
                else
                {
                    // show error alert
                    return $q.all([
                        $translate('ACCOUNT_MSG_UNKNOWN_ERROR_TITLE'),
                        $translate('ACCOUNT_MSG_UNKNOWN_ERROR_TEXT')
                    ])
                    .then(function(translation){
                        $scope.alert = {
                            title: translation[0],
                            text: translation[1] + status,
                            actionsTemplateName: 'close-dialog'
                        };
                    });
                }
            })
            .then(function(){
                $scope.displayAlert();
            });
        }
        
        function loginFb()
        {
            return $q.resolve()
            .then(function(){
                return $q.all([
                    $translate('ACCOUNT_MSG_AWAITING_APPROVAL_FROM_FACEBOOK_TITLE'),
                    $translate('ACCOUNT_MSG_AWAITING_APPROVAL_FROM_FACEBOOK_TEXT'),
                    $translate('BUTTON_CANCEL')
                ]);
            })
            .then(function(translation){
                $scope.alert = {
                    title: translation[0],
                    text: translation[1],
                    actionsTemplateName: 'close-dialog',
                    closeBtnText: translation[2]
                };
                $scope.displayAlert();
            })
            .then(function(){
                return AccountService.loginFb();
            })
            .then(function(status){
                $scope.$closeAllModals();
                if(status == 200)
                {
                    $state.go('home');
                }
                else
                if(status != null)
                {
                    // show error alert
                    console.log(status);
                    return $q.all([
                        $translate('ACCOUNT_MSG_UNKNOWN_ERROR_TITLE'),
                        $translate('ACCOUNT_MSG_UNKNOWN_ERROR_TEXT')
                    ])
                    .then(function(translation){
                        $scope.alert = {
                            title: translation[0],
                            text: translation[1] + status,
                            actionsTemplateName: 'close-dialog'
                        };
                        $scope.displayAlert();
                    });
                }
            });
        }
        
        function signupFb()
        {
            return $q.resolve()
            .then(function(){
                return $q.all([
                    $translate('ACCOUNT_MSG_AWAITING_APPROVAL_FROM_FACEBOOK_TITLE'),
                    $translate('ACCOUNT_MSG_AWAITING_APPROVAL_FROM_FACEBOOK_TEXT'),
                    $translate('BUTTON_CANCEL')
                ]);
            })
            .then(function(translation){
                $scope.alert = {
                    title: translation[0],
                    text: translation[1],
                    actionsTemplateName: 'close-dialog',
                    closeBtnText: translation[2]
                };
                $scope.displayAlert();
            })
            .then(function(){
                return AccountService.signupFb();
            })
            .then(function(status){
                $scope.$closeAllModals();
                if(status == 200)
                {
                    $state.go('home');
                }
                else
                if(status != null)
                {
                    // show error alert
                    return $q.all([
                        $translate('ACCOUNT_MSG_UNKNOWN_ERROR_TITLE'),
                        $translate('ACCOUNT_MSG_UNKNOWN_ERROR_TEXT')
                    ])
                    .then(function(translation){
                        $scope.alert = {
                            title: translation[0],
                            text: translation[1] + status,
                            actionsTemplateName: 'close-dialog'
                        };
                        $scope.displayAlert();
                    });
                }
            });
        }
        
        function logout()
        {
            return AccountService
            .logout()
            .then(function (status){
                if(status == 200)
                {
                    $scope.application.reset();
                    $state.go('entrance');    
                }
            })
        }
        
        function restorePassword(email)
        {
            return $q.resolve()
            .then(function(){
                return $q.all([
                    $translate('ACCOUNT_MSG_LOADING_TITLE'),
                    $translate('ACCOUNT_MSG_LOADING_TEXT')
                ])
            })
            .then(function(translation){
                $scope.alert = {
                    title: translation[0],
                    text: translation[1],
                    actionsTemplateName: '',
                    hideCloseDialogIcon: true
                };
                $scope.displayAlert();
                return AccountService
                .restorePassword(email)
            })
            .then(function(status){
                $scope.$closeAllModals(); 
                if(status == 200)
                {
                    // show success alert
                    return $q.all([
                        $translate('ACCOUNT_MSG_RESTORE_PASSWORD_SUCCESS_TITLE'),
                        $translate('ACCOUNT_MSG_RESTORE_PASSWORD_SUCCESS_TEXT')
                    ])
                    .then(function(translation){
                        $scope.alert = {
                            title: translation[0],
                            text: translation[1],
                            actionsTemplateName: 'close-dialog'
                        };
                        $scope.alert.onCloseIconClick = $scope.alert.closeBtnAction = "$state.go('login')";
                    });
                }
                else
                if(status == 400)
                {
                    return $q.all([
                        $translate('ACCOUNT_MSG_RESTORE_PASSWORD_FAIL_TITLE'),
                        $translate('ACCOUNT_MSG_RESTORE_PASSWORD_FAIL_TEXT')
                    ])
                    .then(function(translation){
                        $scope.alert = {
                            title: translation[0],
                            text: translation[1],
                            actionsTemplateName: 'close-dialog'
                        };
                    });
                }
                else
                {
                    // show error alert
                    return $q.all([
                        $translate('ACCOUNT_MSG_UNKNOWN_ERROR_TITLE'),
                        $translate('ACCOUNT_MSG_UNKNOWN_ERROR_TEXT')
                    ])
                    .then(function(translation){
                        $scope.alert = {
                            title: translation[0],
                            text: translation[1] + status,
                            actionsTemplateName: 'close-dialog'
                        };
                    });
                }
            })
            .then(function(){
                $scope.displayAlert();
            });
        }
    }
})();
