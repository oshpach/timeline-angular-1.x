(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .controller('SearchController', SearchController);
    
    SearchController.$inject = ['$scope', '$state', '$stateParams', '$sce', '$window', 'TimelineService'];
    function SearchController($scope, $state, $stateParams, $sce, $window, TimelineService)
    {
        $scope.events = null;
        
        $scope.cursor = null;
        
        $scope.getEventsList = getEventsList;
        $scope.search = search;
        
        var windowElement = angular.element($window)
        .on('scroll', onPageScroll)
        .on('resize', onPageScroll);
        
        $scope.$on('$destroy', function(){
            windowElement.off('scroll', onPageScroll);
            windowElement.off('resize', onPageScroll);
        });
        
        function search(value){
            $state.go('search', {q: value}, {reload: true});
        }
        
        function getEventsList(){
            var cursor = $scope.cursor;
            $scope.cursor = null;
            if(!$stateParams.q || !$stateParams.q.trim()) return $state.go('home');
            return TimelineService
            .searchEventsEx($stateParams.q.trim(), cursor)
            .then(function(result){
                if(angular.isArray(result.events))
                {
                    $scope.events = ($scope.events || []).concat(result.events);
                    $scope.cursor = result.cursor;
                    if($scope.yearItem == null)
                    {
                        $scope.yearItem = {
                            year: $stateParams.year
                        };
                    }
                    result.events.forEach(function(event){
                        event.html = $sce.trustAsHtml(event.html);
                    });
                }
            });
        }
        
        function onPageScroll(){
            if($window.innerHeight + $window.pageYOffset > $window.document.body.scrollHeight - 100)
            {
                $scope.cursor && $scope.getEventsList();
            }
        }
    }
})();
