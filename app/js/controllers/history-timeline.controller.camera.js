(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .controller('CameraController', CameraController);
    
    CameraController.$inject = ['$scope', '$q', '$document', '$window', '$sce', '$translate', 'TimelineService']
    function CameraController($scope, $q, $document, $window, $sce, $translate, TimelineService)
    {
        console.log('got camera controller');
        $scope.uploadFile = uploadFile;
        
        $scope.resetBlob = resetBlob;
        
        function resetBlob()
        {
            $scope.fileBlob = null;
            $scope.imageUrl = null;
            $scope.videoUrl = null;
        }
        
        function uploadFile()
        {
            var fileInput = angular.element('<input type="file" accept="image/*;capture=camera" style="opacity:0;"/>');
            fileInput.on('change', function(){
                var fileBlob = this.files[0];
                $scope.imageUrl = null;
                $scope.videoUrl = null;
                window.fileBlob = fileBlob;
            
                switch(fileBlob.type)
                {
                    case 'image/jpeg':
                    $scope.imageUrl = $window.URL.createObjectURL(fileBlob);
                    var image = new Image();
                    image.src = $scope.imageUrl;
                    image.onload = function(){
                        $scope.fileBlob = fileBlob;
                        $scope.$apply();    
                    };
                    image.onerror = function(){
                        // display error
                        displayError('CAMERA_MSG_ERROR_INVALID_FORMAT_TITLE', 'CAMERA_MSG_ERROR_INVALID_FORMAT_TEXT');
                        $scope.$apply();
                    };
                    break;
                    case 'video/mp4':
                    $scope.videoUrl = $sce.trustAsResourceUrl($window.URL.createObjectURL(fileBlob));
                    var video = document.createElement('video');
                    video.src = $scope.videoUrl;
                    video.onloadedmetadata = function(){
                        if(fileBlob.size > 500*1024*1024 || video.duration > 3*60)
                        {
                            // display error
                            displayError('CAMERA_MSG_ERROR_VIDEO_SIZE_LIMIT_TITLE', 'CAMERA_MSG_ERROR_VIDEO_SIZE_LIMIT_TEXT');
                        }
                        else 
                        {
                            $scope.fileBlob = fileBlob;
                        }
                        
                        $scope.$apply();
                    };
                    video.onerror = function(){
                        // display error
                        displayError('CAMERA_MSG_ERROR_INVALID_FORMAT_TITLE', 'CAMERA_MSG_ERROR_INVALID_FORMAT_TEXT');
                        $scope.$apply();
                    };
                    break;
                    default:
                    // display error
                    displayError('CAMERA_MSG_ERROR_INVALID_FORMAT_TITLE', 'CAMERA_MSG_ERROR_INVALID_FORMAT_TEXT');
                    break;
                }
                
                $scope.$apply();
            });
            fileInput[0].click();
        }
        
        function displayError(title, text)
        {
            $q.all([
                $translate(title),
                $translate(text)
            ])
            .then(function(translation){
                $scope.alert = {
                    title: translation[0],
                    text: translation[1],
                    actionsTemplateName: 'close-dialog'
                };
                $scope.displayAlert();
            });
        }
    }
})();
