
(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .controller('EntranceScreenController', EntranceScreenController);
    
    EntranceScreenController.$inject = ['$scope', '$state', '$translate', '$timeout', 'AppService'];
    function EntranceScreenController($scope, $state, $translate, $timeout, AppService)
    {
        $scope.getLanguages = getLanguages;
        $scope.getDomains = getDomains;
        $scope.selectDomain = selectDomain;
        $scope.selectLanguage = selectLanguage;
        $scope.onLanguageChange = onLanguageChange;
        
        function onLanguageChange($value, formValues)
        {
            if(!$value) return;
            formValues.domain = null;
            formValues.domainName = null;
            getDomains($value+'');
            selectLanguage($value+'');
        }
        
        function getLanguages()
        {
            AppService.getLanguages()
            .then(function(languages){
                if(angular.isArray(languages))
                {
                    $scope.languages = languages;
                }
                else
                {
                    $scope.languages = null;
                }
            });
        }
        
        function getDomains(language)
        {
            AppService.getDomains(language)
            .then(function(domains){
                if(angular.isArray(domains))
                {
                    $scope.domains = domains;
                }
                else
                {
                    $scope.domains = null;
                }
            });
        }
        
        function selectDomain(langId, domainId, remember)
        {
            AppService.selectDomain(langId, domainId, remember)
            .then(function(){
                $state.go('login', {langId: langId, domainId: domainId});
            });
        }
        
        function selectLanguage(langId)
        {
            $timeout(function(){
                $state.current.name == 'entrance' && $translate.use(langId);
            });
        }
    }
})();
