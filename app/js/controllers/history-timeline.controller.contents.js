(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .controller('ContentsController', ContentsController);
    
    ContentsController.$inject = ['$q','$scope', '$sce', '$state', '$stateParams','$window','$timeout', '$translate', 'TimelineService']
    function ContentsController($q, $scope, $sce, $state, $stateParams,$window,$timeout, $translate, TimelineService)
    {
        $scope.contents = [];
        $scope.cursor = null;
        $scope.fetchContents = fetchContents;
        $scope.toggleLike = toggleLike;
        $scope.shareViaFacebook = shareViaFacebook;
        $scope.shareViaMail = shareViaMail;
        $scope.reportToAdmin = reportToAdmin;
        $scope.createEvent = createEvent;
        $scope.addToTimeline = addToTimeline;
        $scope.submitContent = submitContent;
        $scope.previewContent = previewContent;
        $scope.ContentsController = $scope;
        
        var windowElement = angular.element($window)
        .on('scroll', onPageScroll)
        .on('resize', onPageScroll);
        
        $scope.$on('$destroy', function(){
            windowElement.off('scroll', onPageScroll);
            windowElement.off('resize', onPageScroll);
        });
        
        if($scope.event){}
        else if($scope.application.forceEvent) {
            $scope.event = $scope.application.forceEvent;
        }
        else
            $stateParams.event && TimelineService
            .getEventById($stateParams.event)
            .then(function(result){
                $scope.event = result;
                $scope.application.currentPage.event = result;
            });
        
        function createEvent(event){
            $scope.event = event;
        }
        
        function addToTimeline(){
            $state.go('addToTimeline', {event: $stateParams.event}, {reload: true});
        }
        
        function submitContent(event, headline, details, credits, media, alertDialog, showAlertDialogFn){
            return $q.resolve()
            .then(function(){
                return $q.all([
                    $translate('ADD_CONTENT_PAGE_MSG_UPLOADING_TITLE'),
                    $translate('ADD_CONTENT_PAGE_MSG_UPLOADING_TEXT')
                ]);
            })
            .then(function(translation){
                alertDialog.title = translation[0];
                alertDialog.text = translation[1];
                alertDialog.actionsTemplateName = '';
                alertDialog.hideCloseDialogIcon = true;
                showAlertDialogFn();
            })
            .then(function(){
                return TimelineService
                .addContent(event.id || event, headline, details, credits, media);
            })
            .then(function(result){
                $scope.$closeAllModals();
                alertDialog.icon = null;
                alertDialog.hideTopBar = false;
                alertDialog.closeBtnAction = null;
                alertDialog.hideCloseDialogIcon = false;
                if(result == 200)
                {
                    return $q.all([
                        $translate('ADD_CONTENT_PAGE_MSG_SUCCESS_TITLE'),
                        $translate('ADD_CONTENT_PAGE_MSG_SUCCESS_TEXT')
                    ])
                    .then(function(translation){
                        alertDialog.title = translation[0];
                        alertDialog.text = translation[1];
                        alertDialog.actionsTemplateName = 'upload-ok';
                        alertDialog.icon = '/img/success-icon.png';
                        alertDialog.hideTopBar = true;
                        alertDialog.closeBtnAction = "$state.go('home')";
                    });
                }
                else
                if(result == 400)
                {
                    return $q.all([
                        $translate('ADD_CONTENT_PAGE_MSG_FAIL_TITLE'),
                        $translate('ADD_CONTENT_PAGE_MSG_FAIL_TEXT')
                    ])
                    .then(function(translation){
                        alertDialog.title = translation[0];
                        alertDialog.text = translation[1];
                        alertDialog.actionsTemplateName = 'close-dialog';
                    });
                }
                else
                {
                    return $q.all([
                        $translate('ADD_CONTENT_PAGE_MSG_UKNOWN_ERROR_TITLE'),
                        $translate('ADD_CONTENT_PAGE_MSG_UKNOWN_ERROR_TEXT')
                    ])
                    .then(function(translation){
                        alertDialog.title = translation[0];
                        alertDialog.text = translation[1] + result;
                        alertDialog.actionsTemplateName = 'close-dialog';
                    });
                }
            })
            .then(function(){
                showAlertDialogFn();
            });
        }
        
        function toggleLike(eventId, content){
            return TimelineService
            .toggleLikeContent(eventId, content.id, content.media.type)
            .then(function(status){
                if(status === 200)
                {
                    if(content.userLikedThis)
                        content.likesCounter--;
                    else
                        content.likesCounter++;
                    content.userLikedThis = !content.userLikedThis;    
                }
            });
        }
        
        function previewContent(content)
        {
            alert('TODO: preview content id: ' + content.id);
        }
        
        function shareViaFacebook(content)
        {
            FB.ui({
                method: 'feed',
                name: content.details,
                link: $state.href('content', {id: content.id, type: content.media.type}, {absolute:true}),
                picture: content.media && content.media.url,
                caption: content.credits,
                //description: content.details,
                message: ''
            });
        }
        
        function shareViaMail(args)
        {
            args.alertDialog.processing = true;
            return TimelineService
            .shareContentViaMail(args.eventId, args.contentId, args.contentType, args.recipientName, args.recipientEmail, args.message)
            .then(function(status){
                if(status == 200)
                {
                    return $q.all([
                        $translate('SHARE_VIA_EMAIL_MSG_SUCCESS_TITLE'),
                        $translate('SHARE_VIA_EMAIL_MSG_SUCCESS_TEXT')
                    ])
                    .then(function(translation){
                        args.alertDialog.title = translation[0];
                        args.alertDialog.text = translation[1];
                        args.alertDialog.actionsTemplateName = 'close-all-dialogs';
                        args.alertDialog.onCloseIconClick = '$closeAllModals()';
                    });
                }
                else
                if(status == 400)
                {
                    return $q.all([
                        $translate('SHARE_VIA_EMAIL_MSG_FAIL_TITLE'),
                        $translate('SHARE_VIA_EMAIL_MSG_FAIL_TEXT')
                    ])
                    .then(function(translation){
                        args.alertDialog.title = translation[0];
                        args.alertDialog.text = translation[1];
                        args.alertDialog.actionsTemplateName = 'close-dialog';
                        args.alertDialog.onCloseIconClick = null;
                    });
                }
                else
                {
                    return $q.all([
                        $translate('SHARE_VIA_EMAIL_MSG_UKNOWN_ERROR_TITLE'),
                        $translate('SHARE_VIA_EMAIL_MSG_UKNOWN_ERROR_TEXT')
                    ])
                    .then(function(translation){
                        alertDialog.title = translation[0];
                        alertDialog.text = translation[1] + result;
                        alertDialog.actionsTemplateName = 'close-dialog';
                        alertDialog.onCloseIconClick = null;
                    });
                }
            })
            .then(function(){
                args.showAlertFn();
            })
            .then(function(){
                $timeout(function(){
                    args.alertDialog.processing = false;
                },1000)
            });
        }
        
        function reportToAdmin(args)
        {
            args.alertDialog.processing = true;
            return TimelineService
            .reportToAdmin(args.eventId, args.contentId, args.contentType, args.topicName, args.message)
            .then(function(status){
                if(status == 200)
                {
                    return $q.all([
                        $translate('REPORT_PAGE_MSG_SUCCESS_TITLE'),
                        $translate('REPORT_PAGE_MSG_SUCCESS_TEXT')
                    ])
                    .then(function(translation){
                        args.alertDialog.title = translation[0];
                        args.alertDialog.text = translation[1];
                        args.alertDialog.actionsTemplateName = 'close-all-dialogs';
                        args.alertDialog.onCloseIconClick = '$closeAllModals()';
                    });
                }
                else
                if(status == 400)
                {
                    return $q.all([
                        $translate('REPORT_PAGE_MSG_FAIL_TITLE'),
                        $translate('REPORT_PAGE_MSG_FAIL_TEXT')
                    ])
                    .then(function(translation){
                        args.alertDialog.title = translation[0];
                        args.alertDialog.text = translation[1];
                        args.alertDialog.actionsTemplateName = 'close-dialog';
                        args.alertDialog.onCloseIconClick = null;
                    });
                }
                else
                {
                    return $q.all([
                        $translate('REPORT_PAGE_MSG_UKNOWN_ERROR_TITLE'),
                        $translate('REPORT_PAGE_MSG_UKNOWN_ERROR_TEXT')
                    ])
                    .then(function(translation){
                        args.alertDialog.title = translation[0];
                        args.alertDialog.text = translation[1] + result;
                        args.alertDialog.actionsTemplateName = 'close-dialog';
                        args.alertDialog.onCloseIconClick = null;
                    });
                }
            })
            .then(function(){
                args.showAlertFn();
            })
            .then(function(){
                $timeout(function(){
                    args.alertDialog.processing = false;
                },1000)
            });
        }
        
        function fetchContents(){
            if($stateParams.id)
            {
                return TimelineService
                .getContentById($stateParams.id, $stateParams.type)
                .then(function(content){
                    $scope.contents = [content];
                    return TimelineService
                    .getEventById(content.event)
                    .then(function(event){
                        $scope.event = event;
                    });
                });
            }
            if(!$stateParams.event) return;
            var cursor = $scope.cursor;
            $scope.cursor = null;
            return TimelineService
            .getContents($stateParams.event, cursor)
            .then(function(result){
                if(result.contents.length)
                {
                    $scope.contents = $scope.contents.concat(result.contents);
                    $scope.cursor = result.cursor;
                    result.contents.forEach(function(content){
                        if(content.media && content.media.type === 'video' && content.media.link)
                        {
                            content.media.link = $sce.trustAsResourceUrl(content.media.link);
                        }
                    })
                }
                $timeout(onPageScroll, 100); // emulate scroll if all items displaying in page
            });
        }
        
        function onPageScroll(){
            if($window.innerHeight + $window.pageYOffset > $window.document.body.scrollHeight - 100)
            {
                $scope.cursor && $scope.fetchContents();
            }
        }
    }
})();
