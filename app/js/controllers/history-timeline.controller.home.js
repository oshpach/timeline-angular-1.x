(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .controller('HomeController', HomeController);
    
    HomeController.$inject = ['$scope', '$stateParams', 'TimelineService']
    function HomeController($scope, $stateParams, TimelineService)
    {
        this.scope = $scope;
        $scope.timeline = null;
        $scope.cursor = null;
        
        $scope.fetchTimeline = fetchTimeline;
        
        TimelineService.getTimelineBackground()
        .then(function(background){
            if(background)
            {
                $scope.background = background;
            }
        });
        
        function fetchTimeline()
        {
            var cursor = $scope.cursor;
            $scope.cursor = null;
            TimelineService.getTimeline(cursor, $stateParams.year)
            .then(function(result){
                if(result && result.timeline && result.timeline.length){
                    $scope.timeline = ($scope.timeline || []).concat(result.timeline);
                    $scope.cursor = result.cursor;
                }
            });
        }
    }
})();
