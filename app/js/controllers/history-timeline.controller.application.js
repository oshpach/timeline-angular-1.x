(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .controller('ApplicationController', ApplicationController);
    
    ApplicationController.$inject = ['$scope', '$q', '$state', '$timeout', 'AppService', 'AccountService'];
    function ApplicationController($scope, $q, $state, $timeout, AppService, AccountService)
    {
        $scope.application = {page: {title: ''}, modalStack: 0, domainImageUrl: '', domainName: '' };
        $scope.application.currentPage = $scope.application.page;
        $scope.modalDialogs = [];
        $scope.$closeAllModals = function closeAllModals(result){
            $scope.modalDialogs.reduceRight(function(notUsed, modalScope){
                modalScope.$close(result);
            }, null);
        };
        
        $scope.setupIsolatedScope = function(isolated){
            isolated.application = $scope.application;
            isolated.modalDialogs = $scope.modalDialogs;
            isolated.$closeAllModals = $scope.$closeAllModals;
        };
        
        $scope.$on('$stateChangeSuccess', function(event, newState){
            $scope.application.page.title = newState.pageTitle || '';
        });
        
        $scope.application.reset = function(force){
            AppService.reset(force)
            .then(function(){
                $state.go('entrance');
            });
        };
        
        $scope.application.zoom = 1;
        $scope.application.isMobile = false;
        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent))
        {
            angular.element(window).on('resize',updateScreen);
            
            function updateScreen(){
                $scope.application.isMobile = true;
			    var windowHeight = innerHeight > innerWidth ? innerHeight : innerWidth;
			    var minHeight = 920;
			    var ratio = windowHeight / minHeight;
			    if(windowHeight < minHeight)
			    {
				    $scope.application.zoom = ratio;
			    }
                angular.element(window).off('resize',updateScreen);
                $scope.$apply();
            }
		}
        /*
        AppService
        .loadSavedDomain()
        .then(function(){
            return $q(function(resolve){
                if($state.current.abstract)
                {
                    $scope.$on('$stateChangeStart', function($event,newState){
                        resolve(newState);
                    });
                }
                else
                {
                    resolve($state.current);
                }
            });
        })
        .then(function(targetState){
            if(AppService.currentDomain && targetState.autoredirectIfAuthorised)
            {
                AccountService.hasAuthorisationCookie()
                .then(function(hasIt){
                    if(hasIt)
                        $state.go('home', {
                            domainId: AppService.currentDomain.domainId,
                            langId: AppService.currentDomain.langId
                        });
                    else
                    if(!targetState.anonymousUsersOnly)
                        $state.go('login', {
                            domainId: AppService.currentDomain.domainId,
                            langId: AppService.currentDomain.langId
                        });
                });
            }
        });
        */
    }
})();
