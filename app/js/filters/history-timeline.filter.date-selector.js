(function(){
    'use strict';
    
    angular
    .module('history-timeline')
    .filter('timelineDateSelector', TimelineDateSelectorFilter);
    
    
    
    TimelineDateSelectorFilter.$inject = ['Config'];
    function TimelineDateSelectorFilter(Config)
    {
        
        var dateTypes = {
            day: function(date){
                var days = [];
                var dtCounter = new Date(date);
                var today = new Date();
                var max = today.getFullYear() == dtCounter.getFullYear() && today.getMonth() == dtCounter.getMonth() ? today.getDate() : 31;
                
                dtCounter.setDate(1);
                dtCounter.setHours(0);
                dtCounter.setMinutes(0);
                dtCounter.setSeconds(0);
                dtCounter.setMilliseconds(0);
                
                for(var i = 0; i < max; i++)
                {
                    if(new Date(dtCounter.getTime() + 1000*60*60*24*i).getMonth() != dtCounter.getMonth())
                        break;
                    days.push(i+1);
                }
                return days;
            },
            month: function(date){
                var months = [];
                var today = new Date();
                var max = today.getFullYear() == date.getFullYear() ? today.getMonth()+1 : 12;
                
                for(var i = 0; i < max; i++)
                {
                    months.push(i+1);
                }
                return months;
            },
            year: function(date){
                var years = [];
                var value = date.getFullYear();
                for(var i = 0; i < Config.AppDateSelectorMaxItems && value - i > 0; i++)
                    years.push(value - i);
                return years;
            }
        };
        
        return function(input, dateType){
            var date = new Date(input === null ? Date.now() : input);
            dateType = dateType || 'year';
            
            if(isNaN(date.getTime())) return [];
            return dateTypes[dateType](date);
        };
    }
})();
