(function(){
    "use strict";
    
    angular
    .module('history-timeline')
    .filter('AppKeys', AppKeys);
    
    function AppKeys()
    {
        return function(arr, keyName){
            if(!Array.isArray(arr)) return {};
            var obj = {};
            arr.forEach(function(item){
                obj[item[keyName]] = item;
            });
            
            return obj;
        };
    }
})();