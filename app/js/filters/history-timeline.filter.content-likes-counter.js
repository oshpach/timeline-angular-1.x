(function(){
    'use strict';
    
    angular
    .module('history-timeline')
    .filter('timelineContentLikes', TimelineLikesCounterFilter);
    
    function TimelineLikesCounterFilter()
    {
        return function(input){
            input = input|0;
            
            var format = '';
            
            if(input >= 1000)
            {
                input = input / 1000 | 0;
                format = 'K';
                
                if(input >= 100)
                {
                    input /= 1000.0;
                    format = 'M';
                    
                    if(input >= 10)
                    {
                        input = input | 0;
                    }
                }
            }
            return input.toFixed(1).replace(/\.?0+$/, '') + format;
        };
    }
})();
